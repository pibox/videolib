## Synopsis

VideoLib - Build a library from TheMovieDB and TheTVDB for you local videos.

VideoLib uses filenames found in a configured directory (and recursively) to do lookups in TheMovieDB.org and TheTVDB.com.  The returned data is used to create a local library that can be used by other applications.  Specifically, this is used to generate a database for a USB flash stick of movies and TV shows plugged into a PiBox Media System.

VideoLib is written in Java and should be run on a desktop to generate a USB stick of data to be used by a PiBox Media System.  It should not (and cannot, by default) run on a PiBox-based system because those systems do not support Java applications.

## Build

VideoLib is built on a local Linux box using ant.  It does not use Maven or other higher-level build utilities.

### Local Build

To get a list of build targets, use the following command.

    ant -p

To build locally, use the following command.

    ant videolib

To build locally and run, use the following command.

    ant videolib.run

or 
    ant videolib
    cd ../pkg
    java -jar videolib.jar -?

To build a packaged version (RPM format), use the following command.

    ant rpm

## Installation

VideoLib can be used directly from the source tree but is also packaged in RPM format.  After building look in the ../pkg directory for an .opk file.  This is the file to be installed on an RPM-based system.

To install the package on the target use the following command.

    sudo rpm -ivh <path to RPM file>

## Contributors

To get involved with PiBox, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

0BSD
