package com.ximba.common;

// Java
import java.io.*;
import java.util.*;
import org.apache.log4j.*;
import org.apache.log4j.spi.*;

/**
 * <p> 
 * Simple log4j appender class that forwards log messages to registered callbacks. 
 * </p>
 * @author Michael J. Hammel
 * @since 1.0
 */

public class LogAppender extends AppenderSkeleton{

    /** The list of registered callbacks for this appender. */
    static Set<LogCallback> callbacks = new HashSet<LogCallback>();

    /** 
     * Register a callback to be called when new log data arrives. Duplicate callbacks are not allowed.
     * @param cb The LogCallback object reference.  The append() method in the callback is called when new log data arrives.
     * @return True if the reference could be added, false if it is already registered.
     */
    public static boolean register(LogCallback cb) throws NullPointerException { 
        if ( cb == null )
            throw new NullPointerException();

        boolean rc = false;
        synchronized ( callbacks )
        {
            rc = callbacks.add(cb); 
        }
        return rc;
    }

    /** 
     * Remove a callback.
     * @param cb The LogCallback object reference.
     * @return True if the reference could be removed, false if it was not currently registered.
     */
    public static boolean remove(LogCallback cb) throws NullPointerException { 
        if ( cb == null )
            throw new NullPointerException();

        boolean rc = false;
        synchronized ( callbacks )
        {
            rc = callbacks.remove(cb);
        }
        return rc;
    }

    /** 
     * Get the size of the callback list.
     * @return The number of callbacks currently registered.
     */
    public static int size() { 
        int size = 0;
        synchronized ( callbacks )
        {
            size = callbacks.size(); 
        }
        return size;
    }

    /** 
     * Reset the callback list.
     */
    public static void reset() { 
        synchronized ( callbacks )
        {
            callbacks.clear(); 
        }
    }

    /**
     * Handler for inbound log4j events.  It iterates over the registered callbacks, calling them one at a time and passing
     * the formatted event message.  To avoid delays callbacks should be very fast, probably by copying the message to a local 
     * thread queue and then returning.
     */
    protected void append(LoggingEvent event) {

        /* If no layout is available, don't do anything. */
        if( this.layout == null ) {   
            errorHandler.error("No layout for appender " + name, null, ErrorCode.MISSING_LAYOUT );
            return;
        }

        synchronized ( callbacks )
        {
            Iterator it = callbacks.iterator();
            while (it.hasNext())
            {
                LogCallback cb = (LogCallback)it.next();

                // Prefix our IP to the message because the registered callback might forward to another agent.
                cb.append(event);
            }
        }
    }

    /**
     * Override requiresLayout() to return true.
     */
    public boolean requiresLayout() { return true; }

    /**
     * Clear out callbacks list on closing of log4j.
     */
    public void close() { 
        synchronized ( callbacks )
        {
            callbacks.clear(); 
        }
    }
}

