package com.ximba.videolib;

/* Java */
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.file.*;
import java.util.*;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import java.util.regex.Pattern;

/* JSON-Simple */
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/* Ximba */
import com.ximba.common.RandomGUID;

/**
 * <p> TheMovieDB API object that holds fields from a JSON return string. </p>
 * @author Michael J. Hammel
 * @since 1.0
 */
public class MovieDB implements Serializable {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.videolib.MovieDB");

    /* Names used in JSON specific to VideoLib. */
    private static final String VACTIVE     = "videolib_active";
    private static final String VFILENAME   = "videolib_filename";
    private static final String VTITLE      = "videolib_title";
    private static final String VVIDEOPATH  = "videolib_path";
    private static final String VMIMAGE     = "videolib_movie_image";
    private static final String VOVERVIEW   = "videolib_overview";

    // ID of the matching movie.
    private String id            = null;
    private String release_date  = null;
    private String overview      = null;
    private String poster_path   = null;
    private String video_path    = null;

    // Movie Title (user-readable)
    String movieTitle = null;

    // Query string
    String queryString = null;

    // JSON results
    String jsonResult = null;

    /*
     * ---------------------------------------------------------------
     * Constructors
     * ---------------------------------------------------------------
     */

    public MovieDB( String title ) throws Exception
    { 
        File file = new File(title);
        if ( file.exists() )
        {
            loadFile(title);
            return;
        }
        if ( title == null )
            throw new Exception("Missing title.");
        movieTitle = title;
        buildSearchQuery(title);
    }

    /*
     * ---------------------------------------------------------------
     * Inner classes
     * ---------------------------------------------------------------
     */

    /*
     * ---------------------------------------------------------------
     * Private methods: Common
     * ---------------------------------------------------------------
     */
    private String readFile( String filename ) throws Exception
    {
        FileReader fr = new FileReader(filename);
        BufferedReader in = new BufferedReader(fr);
        String line = in.readLine();
        fr.close();
        return (line);
    }

    /*
     * Load an existing database file.
     */
    @SuppressWarnings("unchecked")
    private void loadFile( String filename ) throws Exception
    {
        try {
            /* Slurp the file. */
            jsonResult = readFile(filename);

            /* Get the active ID and load it. */
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(jsonResult);
            String active = (String)jsonObject.get(VACTIVE);
            String video_path = (String)jsonObject.get(VVIDEOPATH);
            String overview = (String)jsonObject.get(VOVERVIEW);
            setID(active);

            /* Extract our version of the movie name */
            movieTitle = (String)jsonObject.get(VTITLE);
            buildSearchQuery(movieTitle);
        }
        catch(Exception e){
            log.error("Failed to load file " + filename, e);
        }
    }

    // Build a query based on a movie or TV name.  We do a simple search, nothing more.
    // API: https://www.themoviedb.org/documentation/api
    // Example: http://stackoverflow.com/questions/14152276/themoviedb-json-api-with-jquery
    private void buildSearchQuery( String title ) throws Exception
    {
        Cli cli = new Cli();
        String apiKey = cli.get(Cli.S_APIKEY);
        if ( apiKey == null )
        {
            log.error("Missing API Key - can't query tmdb.org");
            return;
        }
        StringBuilder url = new StringBuilder("http://api.themoviedb.org/3/search/movie");
        url.append ( "?api_key=" + apiKey );
        url.append ( "&query=" + URLEncoder.encode(title, "UTF-8") );
        queryString = url.toString();
    }

    /*
     * ---------------------------------------------------------------
     * Private methods: Movies
     * ---------------------------------------------------------------
     */

    // Perform a query for a movie 
    private void queryDBMovie()
    {
        log.info("Trying: " + queryString);
        String jsonStr = makeRequest( queryString );
        if ( jsonStr != null )
        {
            long resultCount = getResultCount(jsonStr);
            if ( resultCount != 0 )
            {
                jsonResult = jsonStr;
                log.info("JSON: " + jsonStr);
                parseMovieQuery( jsonStr );
                String url = buildImageQuery();
                requestImage(url);
                return;
            }
        }
        log.info("No match found.");
    }

    // Parse a JSON string for an individual movie.
    @SuppressWarnings("unchecked")
    private void parseMovieQuery( String jsonStr )
    {
        try {
            //parses input json string to JSONObject
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(jsonStr);

            // Retrieve the array of results.
            JSONArray results = null;
            try { results = (JSONArray)jsonObject.get("results"); }
            catch(Exception e) {}
            if ( results == null )
            {
                log.error ("parseMovieQuery: Missing results field in JSON data.");
                return;
            }

            // Get first result. It's the most likely match.
            Iterator it = results.iterator();
            long id = -1;
            while( it.hasNext() )
            {
                JSONObject obj = (JSONObject)it.next();

                release_date = (String) obj.get( "release_date" );
                id = (long) obj.get("id");
                overview = (String) obj.get( "overview" );
                poster_path = (String)obj.get("poster_path");
                log.info("parseMovieQuery: poster_path: " + poster_path);

                if ( id != -1 )
                    break;
            }

            // Save ID
            if ( id != -1 )
            {
                this.id = ""+id;
                jsonObject.put(VACTIVE, this.id);
                jsonObject.put(VTITLE, this.movieTitle);
                jsonResult = jsonObject.toJSONString();
                log.info("Original JSON: \n" + jsonResult);
            }
            else
                log.error("Couldn't find ID");

        } catch (ParseException e) {
            log.info("Failed to parse JSON", e);
            e.printStackTrace();
        }
    }

    /*
     * ---------------------------------------------------------------
     * Public Methods: Common
     * ---------------------------------------------------------------
     */

    /*
     * Query the upstream database for the configured movie title.
     */
    public void queryDB()
    {
        queryDBMovie();
    }

    /*
     * Change to an alternative entry based on another ID.
     */
    public void setPath( String path )
    {
        this.video_path = path;
    }

    /*
     * Make a request to the upstream database.
     */
    public String makeRequest(String request)
    {
        BufferedReader in = null;
        try {
            URL url = new URL(request); 
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();           
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(false); 
            connection.setRequestMethod("GET"); 
            connection.setUseCaches (false);
            connection.disconnect();
        
            // Pull the results
            in = new BufferedReader( new InputStreamReader(connection.getInputStream()) );
            String decodedString;
            StringBuilder response = new StringBuilder("");
            while ((decodedString = in.readLine()) != null) {
                response.append(decodedString + "\n");
            }
            log.info("Response: " + response.toString());
            if ( response.toString().length() == 0 )
            {
                log.error("No data in response from TheMovieDB.org.  Skipping.");
                return null;
            }
            return response.toString();
        }
        catch(Exception e)
        {
            log.error("Error making HTTP request: " + e.getMessage(), e);
            return null;
        }
        finally {
            try {
                if ( in != null )
                    in.close();
            }
            catch(Exception e){}
        }
    }

    /*
     * Parse JSON return value from TheMovieDB query.  Results are kept
     * internally to this class.
     */
    public void parseQuery( String jsonStr )
    {
        parseMovieQuery( jsonStr );
    }

    public String getID() { return this.id; }
    public String getOverview() { return this.overview; }
    public String getReleaseDate() { return this.release_date; }
    public String getPosterPath() { return this.poster_path; }
    public String getTitle() { return this.movieTitle; }
    public String getPath() { return this.video_path; }

    /*
     * ---------------------------------------------------------------
     * Public Methods: Movie
     * ---------------------------------------------------------------
     */

    /*
     * Download an image from the upstream provider.
     */
    public void requestImage(String urlStr)
    {
        Cli cli = new Cli();
        if ( cli.get(Cli.S_MOVIEDIR) == null )
        {
            log.error("Movie dir is not set.  Skipping request for poster.");
            return;
        }

        /*
         * Make sure we have a place to store the image.
         */
        String posterPath = cli.get(Cli.S_MOVIEDIR) + File.separator + "posters" ;
        File dir = new File ( posterPath );
        dir.mkdirs();

        /*
         * Build an output filename.
         */
        String fileName = posterPath + File.separator + poster_path;
        log.info("Image filename: " + fileName);
        log.info("Request URL: " + urlStr);
        try {
            URL url = new URL(urlStr); 
            BufferedImage image = ImageIO.read(url);
            if ( ImageIO.write(image, "png", new File( fileName ) ) )
                return;
            else
                log.error("Failed write - no image appropriate write available.");
        }
        catch(Exception e)
        {
            log.error("Error making HTTP request: " + e.getMessage(), e);
            return;
        }
    }

    /*
     * Build a query with a specific movie ID.  We do a simple search, nothing more.
     * API: https://www.themoviedb.org/documentation/api
     * Example: http://stackoverflow.com/questions/14152276/themoviedb-json-api-with-jquery
     */
    public String buildIDQuery( String movieID )
    {
        Cli cli = new Cli();
        String apiKey = cli.get(Cli.S_APIKEY);
        if ( apiKey == null )
        {
            log.error("Missing API Key - can't query tmdb.org");
            return null;
        }
        StringBuilder url = new StringBuilder("http://api.themoviedb.org/3/movie/");
        url.append ( movieID + "?" );
        url.append ( "api_key=" + apiKey );
        return url.toString();
    }

    /*
     * Build image query.  This is hard coded but should use the "configuration" option to
     * find the correct base_url.
     * API: https://www.themoviedb.org/documentation/api
     * Example: http://stackoverflow.com/questions/14152276/themoviedb-json-api-with-jquery
     */
    public String buildImageQuery()
    {
        if ( poster_path == null )
        {
            log.error("No poster path available - can't build image query.");
            return null;
        }
        Cli cli = new Cli();
        String apiKey = cli.get(Cli.S_APIKEY);
        if ( apiKey == null )
        {
            log.error("Missing API Key - can't query tmdb.org");
            return null;
        }

        StringBuilder url = new StringBuilder("http://image.tmdb.org/t/p");
        url.append ( "/original" );
        url.append ( poster_path + "?" );
        url.append ( "api_key=" + apiKey );
        String newURL = url.toString();
        return url.toString();
    }

    /*
     * Parse JSON to find number of results
     */
    public long getResultCount( String jsonStr )
    {
        try {
            //parses input json string to JSONObject
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(jsonStr);

            // Overview
            long totalResults = (long)jsonObject.get("total_results");
            log.info ("total_results: " + totalResults);
            return totalResults;
        } catch (ParseException e) {
            log.info("Failed to parse JSON", e);
            e.printStackTrace();
            return 0;
        }
    }

    /*
     * Retrieve a list of alternate titles and associated IDs
     */
    public java.util.List<String> getAlternatives()
    {
        java.util.List<String> list = new java.util.ArrayList<String>();

        if ( jsonResult == null )
            return null;

        try {
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(jsonResult);

            // Retrieve the array of results.
            JSONArray results = null;
            try { results = (JSONArray)jsonObject.get("results"); }
            catch(Exception e) {}
            if ( results == null )
            {
                log.error ("getAlternatives: Missing results field in JSON data.");
                return list;
            }

            // Add each entry to the list.
            Iterator it = results.iterator();
            while( it.hasNext() )
            {
                JSONObject obj = (JSONObject)it.next();

                String altID    = ""+obj.get("id");
                String altDate  = (String) obj.get( "release_date" );
                String altTitle = (String) obj.get("title");

                if ( !altID.equals(id) )
                {
                    list.add( altID + ":" + altDate + ":" + altTitle );
                }
            }
        } catch (ParseException e) {
            log.info("Failed to parse JSON", e);
            e.printStackTrace();
        }
        return list;
    }

    /*
     * Change to an alternative entry based on another ID.
     */
    @SuppressWarnings("unchecked")
    public void setID( String newID )
    {
        if ( jsonResult == null )
        {
            log.error("Missing JSON data for this movie: " + movieTitle);
            return;
        }

        try {
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(jsonResult);

            // Retrieve the array of results.
            JSONArray results = null;
            try { results = (JSONArray)jsonObject.get("results"); }
            catch(Exception e) {}
            if ( results == null )
            {
                log.error ("getAlternatives: Missing results field in JSON data.");
                return;
            }

            // Add each entry to the list.
            Iterator it = results.iterator();
            while( it.hasNext() )
            {
                JSONObject obj = (JSONObject)it.next();

                String altID = ""+obj.get("id");
                if ( (altID != null) && altID.equals(newID) )
                {
                    jsonObject.put(VACTIVE, altID);
                    this.id = altID;
                    release_date = (String) obj.get( "release_date" );
                    overview = (String) obj.get( "overview" );
                    poster_path = (String)obj.get("poster_path");
                    Cli cli = new Cli();
                    String posterPath = cli.get(Cli.S_MOVIEDIR) + File.separator + "posters" ;
                    String fileName = posterPath + File.separator + poster_path;
                    File file = new File( fileName );
                    if ( !file.exists() )
                    {
                        String url = buildImageQuery();
                        requestImage(url);
                    }
                    jsonResult = jsonObject.toJSONString();
                    log.info("Updated JSON: \n" + jsonResult);
                    return;
                }
            }
        } catch (ParseException e) {
            log.info("Failed to parse JSON", e);
            e.printStackTrace();
        }
    }

    /*
     * Save this entry to the specified directory.
     */
    @SuppressWarnings("unchecked")
    public void save( String basepath, String dirname )
    {
        File dir = new File (dirname);
        if ( !dir.exists() )
        {
            log.error("No such directory: " + dirname);
            return;
        }
        try {
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(jsonResult);

            /* 
             * Save our version of the movie title.
             * This should match the name we found for this movie 
             * when scanning the directories.
             */
            jsonObject.put(VTITLE, movieTitle);

            /*
             * Save path to the video file.  Makes it easier for
             * players to find the file.
             */
            jsonObject.put(VVIDEOPATH, video_path.replace(basepath, ""));

            /*
             * Save path to move image.
             */
            jsonObject.put(VMIMAGE, poster_path);

            /*
             * Save the overview.
             */
            jsonObject.put(VOVERVIEW, overview);

            /* Get our filename, if we have one. */
            String filename = (String)jsonObject.get(VFILENAME);
            if ( filename == null )
            {
                /*
                 * We use a UUID to identify this entry.
                 * We put the UUID in the entry so we always 
                 * use the same file no matter which alternative
                 * we choose.
                 */
                filename = new RandomGUID().toString();
                log.info("Video filename: "+ filename);
                jsonObject.put(VFILENAME, filename);
            }

            String path = dirname + File.separator + filename;
            PrintWriter out = new PrintWriter(path);
            out.println( jsonObject.toJSONString() );
            out.close();
        }
        catch(Exception e) {
            log.info("Failed to save MovieDB JSON data.", e);
        }
    }
}
