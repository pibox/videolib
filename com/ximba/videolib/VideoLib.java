package com.ximba.videolib;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.file.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/* Project Imports */
import com.ximba.common.*;

/**
 * <p> 
 * Main class for VideoLib.
 * </p>
 * @author Michael J. Hammel
 * @since 1.0
 */

class VideoLib {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.videolib.VideoLib");

    // Where we store log4j properties, relative to runtime directory.
    private static final String S_LOG4JPROPS = "config/log4j.properties";

    // Run time Configuration 
    Cli cli = null;

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    public VideoLib() {}

    /*
     * =======================================================
     * Private methods
     * =======================================================
     */

    private void parseCmdLine( String[] args )
    {
        cli = new Cli();
        cli.parseCmdLine( args );
        cli.load();
    }

    private void setup()
    {
        System.err.println("Version: " + BuildInfo.getVersion());
        if ( cli.isVerbose() ) 
        {
            System.err.println("Verbose: Enabled");
        }

        // Deal with signals - not pretty but best case for Java.
        final Thread mainThread = Thread.currentThread();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                Cli cli = new Cli() ;
                cli.disable() ;
                try { mainThread.join(); }
                catch(Exception e) {}
            }
        });

        Cli cli = new Cli();
    }

    private void go()
    {
        // start UI Thread
        UI ui = new UI();
        ui.setup();
        try { ui.start(); }
        catch(Exception e){}

        // Enable processing and wait for exit.
        Utils.initTimeStamp();
        cli.enable();
        while( cli.isEnabled() )
        {
            try { Thread.sleep(25); }
            catch(Exception e) {}
        }

        // stop UI Thread
        ui.shutdown();

        log.info("VideoLib shutdown complete.");
    }

    /*
     * =======================================================
     * Main
     * Launch threads.
     * Then sit and spin till we get a shutdown notification.
     * =======================================================
     */
    public static void main(String[] args) {
        // Load log4j properties file.  It must be in a file relative to where we started.
        PropertyConfigurator.configure(S_LOG4JPROPS);

        VideoLib videolib = new VideoLib();
        videolib.parseCmdLine( args );
        videolib.setup();
        videolib.go();
    }
}
