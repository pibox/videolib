package com.ximba.videolib;

/* Java */
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import java.util.regex.Pattern;

/* JSON-Simple */
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/* Ximba */
import com.ximba.common.RandomGUID;

/**
 * <p> TheMovieDB API object that holds fields from a JSON return string. </p>
 * @author Michael J. Hammel
 * @since 1.0
 */
public class TVDB implements Serializable {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.videolib.TVDB");

    /* Names used in JSON specific to VideoLib. */
    private static final String VSERIESID   = "videolib_active_series";
    private static final String VEPISODEID  = "videolib_active_episode";
    private static final String VFILENAME   = "videolib_filename";
    private static final String VETITLE     = "videolib_episode_title";
    private static final String VRTITLE     = "videolib_real_title";
    private static final String VVIDEOPATH  = "videolib_path";
    private static final String VEIMAGE     = "videolib_episode_image";
    private static final String VSIMAGE     = "videolib_series_image";
    private static final String VOVERVIEW   = "videolib_overview";

    /*
     * Base URLs
     * SERIES is a GET
     * http://api.themoviedb.org/3/search/tv - retrieve "id" (SouthPark == 2190)
     * http://api.themoviedb.org/3/tv/2190/season/2/episode/1?api_key=cf67af93299108a487074a5811ab41ed&language=en-US
     * JSON return: "name" and "overview" have episode title and summary. "id" has the episode id.
     * Images: see buildImageQuery() - use "still_path" field in episode JSON for episode image.
     * Images: see buildImageQuery() - use "backdrop_path" or "poster_path" field in series JSON for series image.
     */
    private static final String SERIES_DETAILS_URL = "https://api.thetvdb.com/series";
    private static final String EPISODE_URL        = "https://api.thetvdb.com/episodes";
    private static final String SERIES_IMAGE_URL   = "http://thetvdb.com/banners/_cache/posters/";
    private static final String EPISODE_IMAGE_URL  = "http://thetvdb.com/banners/episodes/";

    // ID of the matching episode.
    private String seriesID      = null;
    private String episodeID     = null;
    private String release_date  = null;
    private String overview      = null;
    private String episode_image = null;
    private String series_image  = null;
    private String video_path    = null;

    // episode Title (user-readable)
    String episodeTitle = null;
    String seriesTitle = null;
    String season = null;
    String episodeNum = null;
    String episodeRealTitle = null;

    // Series and episode JSON
    String seriesJSON = null;
    String episodeSummaryJSON = null;
    String episodeJSON = null;
    String seriesDetailsJSON = null;

    // Query string
    String queryString = null;

    // JSON results
    String jsonResult = null;

    /*
     * ---------------------------------------------------------------
     * Constructors
     * ---------------------------------------------------------------
     */

    public TVDB( String title ) throws Exception
    {
        File file = new File(title);
        if ( file.exists() )
        {
            log.info("Loading file: " + title);
            loadFile(title);
            return;
        }

        if ( title == null )
            throw new Exception("Missing title.");
        episodeTitle = title;

        /* Extract required fields. */
        parseFields();
    }

    /*
     * ---------------------------------------------------------------
     * Inner classes
     * ---------------------------------------------------------------
     */

    /*
     * ---------------------------------------------------------------
     * Private methods
     * ---------------------------------------------------------------
     */

    /*
     * Read a file from disk.
     */
    private void readFile( String filename ) throws Exception
    {
        FileReader fr = new FileReader(filename);
        BufferedReader in = new BufferedReader(fr);
        seriesJSON = in.readLine();
        episodeJSON = in.readLine();
        seriesDetailsJSON = in.readLine();
        fr.close();
    }

    /*
     * Load an existing database file.
     */
    @SuppressWarnings("unchecked")
    private void loadFile( String filename ) throws Exception
    {
        try {
            /* Slurp the file. */
            readFile(filename);

            /*
             * Parse seriesJSON
             */
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(seriesJSON);

            /*
             * Get the path to the video.
             */
            video_path = (String)jsonObject.get(VVIDEOPATH);

            /* Get the active ID and load it. */
            String active = (String)jsonObject.get(VSERIESID);
            parseSeries(active);

            /* Extract our version of the episode name */
            episodeID = (String)jsonObject.get(VEPISODEID);
            episodeTitle = (String)jsonObject.get(VETITLE);
            episodeRealTitle = (String)jsonObject.get(VRTITLE);

            /* Load the overview */
            overview = (String)jsonObject.get(VOVERVIEW);

            // series_image = seriesID + "-1.jpg";
            // episode_image = episodeID + ".jpg";
        }
        catch(Exception e){
            log.error("Failed to load file " + filename, e);
        }
    }

    // Build a generic search query for a TV series.  We do a simple search, nothing more.
    // API: https://www.themoviedb.org/documentation/api
    // Example: http://stackoverflow.com/questions/14152276/themoviedb-json-api-with-jquery
    private String buildSearchQuery( String title ) throws Exception
    {
        Cli cli = new Cli();
        String apiKey = cli.get(Cli.S_APIKEY);
        if ( apiKey == null )
        {
            log.error("Missing API Key - can't query moviedb.org");
            return null;
        }
        StringBuilder url = new StringBuilder("http://api.themoviedb.org/3/search/tv");
        url.append ( "?api_key=" + apiKey );
        url.append ( "&query=" + URLEncoder.encode(title, "UTF-8") );
        return url.toString();
    }

    // Build a query for a specific TV series ID. 
    private String buildSeriesQuery( String tvid ) throws Exception
    {
        Cli cli = new Cli();
        String apiKey = cli.get(Cli.S_APIKEY);
        if ( apiKey == null )
        {
            log.error("Missing API Key - can't query moviedb.org");
            return null;
        }
        StringBuilder url = new StringBuilder("http://api.themoviedb.org/3/tv/" + tvid);
        url.append ( "?api_key=" + apiKey );
        return url.toString();
    }

    // Build a query for a specific TV episode ID. 
    // /tv/{tv_id}/season/{season_number}/episode/{episode_number}
    private String buildEpisodeQuery( String tvID, String season, String episodeNum ) throws Exception
    {
        Cli cli = new Cli();
        String apiKey = cli.get(Cli.S_APIKEY);
        if ( apiKey == null )
        {
            log.error("Missing API Key - can't query moviedb.org");
            return null;
        }
        StringBuilder url = new StringBuilder("http://api.themoviedb.org/3/tv/" + tvID);
        url.append ( "/season/" + season );
        url.append ( "/episode/" + episodeNum );
        url.append ( "?api_key=" + apiKey );
        return url.toString();
    }

    /*
     * Build image query.
     */
    public String buildImageQuery(String poster_path)
    {
        if ( poster_path == null )
        {
            log.error("No poster path available - can't build image query.");
            return null;
        }
        Cli cli = new Cli();
        String apiKey = cli.get(Cli.S_APIKEY);
        if ( apiKey == null )
        {
            log.error("Missing API Key - can't query tmdb.org");
            return null;
        }

        StringBuilder url = new StringBuilder("http://image.tmdb.org/t/p");
        url.append ( "/original" );
        url.append ( poster_path + "?" );
        url.append ( "api_key=" + apiKey );
        String newURL = url.toString();
        return url.toString();
    }

    /*
     * Parse the filename into it's component parts.
     * Note:  We require the following name format:
     * Series_name - s01e01 ...
     */
    @SuppressWarnings("unchecked")
    private void parseFields() throws Exception
    {
        String[] field = episodeTitle.split("-");
        if (field.length < 2 )
        {
            log.error("Bad format for episode title: " + episodeTitle);
            return;
        }
        seriesTitle = new String(field[0].trim());
        season = new String( field[1].trim().substring(1,3) );
        episodeNum = new String( field[1].trim().substring(4,6) );
        log.info("Series: " + seriesTitle + ", season " + season + ", episode " + episodeNum);
    }

    /*
     * Retrieve info for the episode
     */
    @SuppressWarnings("unchecked")
    private String queryUpstream(URL url)
    {
        /*
        Cli cli = new Cli();
        String apiKey = cli.get(Cli.S_TVAPIKEY);
        if ( apiKey == null )
        {
            log.error("Missing TV API Key - can't query moviedb.com");
            return null;
        }
        */

        BufferedReader in = null;
        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.setUseCaches (false);
            connection.disconnect();

            // Pull the results
            in = new BufferedReader( new InputStreamReader(connection.getInputStream()) );
            String decodedString;
            StringBuilder response = new StringBuilder("");
            while ((decodedString = in.readLine()) != null) {
                response.append(decodedString + "\n");
            }
            if ( response.toString().length() == 0 )
            {
                log.error("No data in response from MovieDB.com.  Skipping.");
                return null;
            }
            return response.toString();
        }
        catch(Exception e)
        {
            log.error("Error retrieving episode: " + e.getMessage(), e);
        }
        finally {
            try {
                if ( in != null )
                    in.close();
            }
            catch(Exception e){}
        }
        return null;
    }

    /* Parse the series data to find required fields. */
    @SuppressWarnings("unchecked")
    private void parseSeries( String wantID )
    {
        try {
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(seriesJSON);

            // Retrieve the array of results.
            JSONArray results = null;
            try { results = (JSONArray)jsonObject.get("results"); }
            catch(Exception e) {}
            if ( results == null )
            {
                log.error ("parseSeries: Missing results field in JSON data.");
                return;
            }

            Iterator it = results.iterator();
            boolean foundIt = false;
            while( it.hasNext() )
            {
                JSONObject obj = (JSONObject)it.next();
                long id = (long) obj.get("id");
                if ( (wantID == null) || (wantID.equals( ""+id )) )
                {
                    release_date = (String) obj.get( "first_air_date" );
                    overview = (String) obj.get( "overview" );
                    this.seriesID = "" + id;
                    this.series_image = (String) obj.get( "poster_path" );
                    foundIt = true;
                    break;
                }
            }

            // Save ID
            if ( foundIt )
            {
                jsonObject.put(VSERIESID, this.seriesID);
                jsonObject.put(VETITLE, this.episodeTitle);
                log.info("Series ID: " + this.seriesID );
            }
            else
                log.error("Couldn't find ID");

        } catch (ParseException e) {
            log.info("Failed to parse JSON", e);
            e.printStackTrace();
        }
    }

    /* Parse the episode data to find required fields. */
    @SuppressWarnings("unchecked")
    private void parseEpisode( String episodeID )
    {
        try {
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(episodeJSON);

            long id = (long) jsonObject.get("id");
            this.episodeID = "" + id;
            this.release_date = (String) jsonObject.get( "air_date" );
            String episode_overview = (String)jsonObject.get("overview");
            if ( (episode_overview != null) && (episode_overview.length() > 0) )
            {
                this.overview = episode_overview;
            }
            String episode_name = (String)jsonObject.get("name");
            if ( (episode_name != null) && (episode_name.length() > 0) )
            {
                this.episodeRealTitle = episode_name;
            }
            this.episode_image = (String)jsonObject.get("still_path");

            jsonObject.put(VEPISODEID, this.episodeID);
            log.info("Episode ID: " + this.episodeID );

        } catch (ParseException e) {
            log.info("Failed to parse JSON", e);
            e.printStackTrace();
        }
    }

    private void requestImage(String urlStr, String imagename)
    {
        Cli cli = new Cli();
        if ( cli.get(Cli.S_MOVIEDIR) == null )
        {
            log.error("episode dir is not set.  Skipping request for poster.");
            return;
        }

        /*
         * Make sure we have a place to store the image.
         */
        String posterPath = cli.get(Cli.S_MOVIEDIR) + File.separator + "posters" ;
        File dir = new File ( posterPath );
        dir.mkdirs();

        /*
         * Build an output filename.
         */
        String fileName = posterPath + File.separator + imagename;
        log.info("Image filename: " + fileName);
        log.info("Request URL: *" + urlStr + "*");

        /*
         * Go and get the image.
         */
        try {
            URL url = new URL(urlStr);
            BufferedImage image = ImageIO.read(url);
            if ( ImageIO.write(image, "png", new File( fileName ) ) )
                return;
            else
                log.error("Failed write - no image appropriate write available.");
        }
        catch(Exception e)
        {
            log.error("Error making HTTP request: " + e.getMessage(), e);
            return;
        }
    }

    /*
     * This fills in all required fields and retrieves required data
     * based on the series selected.
     */
    private void updateEntry()
    {
        URL url;
        String request;

        try {
            request = buildSeriesQuery( seriesID );
            url = new URL(request); 
            seriesDetailsJSON = queryUpstream(url);
            log.info("Series details: " + seriesDetailsJSON);

            request = buildEpisodeQuery(seriesID, season, episodeNum);
            log.info("Episode Query: " + request);
            url = new URL(request); 
            episodeJSON = queryUpstream(url);
            log.info("Episode Info: " + episodeJSON);

            parseEpisode(null);

            // Get series image
            requestImage(buildImageQuery(series_image), series_image);

            // Get episode image
            // http://thetvdb.com/banners/episodes/<seriesID>/<episodeID>.jpg
            requestImage(buildImageQuery(episode_image), episode_image);
        }
        catch(Exception e) {
            log.error("Error retrieving series/episode info", e);
        }
    }

    /*
     * ---------------------------------------------------------------
     * Public Methods
     * ---------------------------------------------------------------
     */

    /*
     * Query the upstream database for the configured episode title.
     */
    public void queryDB()
    {
        try {
            String request = buildSearchQuery( seriesTitle );
            log.info("Series Query: " + request);
            URL url = new URL(request); 
            seriesJSON = queryUpstream(url);
            log.info("Series info: " + seriesJSON);
            parseSeries(null);
            updateEntry();
        }
        catch(Exception e) {
            log.error("Error retrieving series/episode info", e);
        }
    }

    /*
     * Change to an alternative entry based on another ID.
     */
    public void setPath( String path )
    {
        this.video_path = path;
    }

    public String getSeriesID() { return this.seriesID; }
    public String getOverview() { return this.overview; }
    public String getReleaseDate() { return this.release_date; }
    public String getEpisodeImage() { return this.episode_image; }
    public String getSeriesImage() { return this.series_image; }
    public String getTitle() { return this.episodeTitle; }
    public String getRealTitle() { return this.episodeRealTitle; }
    public String getPath() { return this.video_path; }

    /*
     * Set the real title, which is the title we use for display purposes.
     */
    public void setRealTitle( String value )
    { 
        this.episodeRealTitle = value;
    }

    /*
     * Retrieve a list of alternate titles and associated IDs
     */
    public java.util.List<String> getAlternatives()
    {
        java.util.List<String> list = new java.util.ArrayList<String>();

        if ( seriesJSON == null )
            return null;

        try {
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(seriesJSON);

            // Retrieve the array of results.
            JSONArray results = null;
            try { results = (JSONArray)jsonObject.get("results"); }
            catch(Exception e) {}
            if ( results == null )
            {
                log.error ("getAlternatives: Missing results field in JSON data.");
                return list;
            }

            // Add each entry to the list.
            Iterator it = results.iterator();
            while( it.hasNext() )
            {
                JSONObject obj = (JSONObject)it.next();

                String altID    = ""+obj.get("id");
                String altDate  = (String) obj.get( "first_air_date" );
                String altTitle = (String) obj.get("name");
                altTitle = altTitle.replace("\\u0026", "&");

                if ( !altID.equals(seriesID) )
                {
                    String altEntry = altID + ":" + altDate + ":" + altTitle;
                    log.info("Alternative entry = " + altEntry);
                    list.add( altEntry );
                }
            }
        } catch (ParseException e) {
            log.info("Failed to parse JSON", e);
            e.printStackTrace();
        }
        return list;
    }

    /*
     * Change to an alternative entry based on another ID.
     */
    @SuppressWarnings("unchecked")
    public void setID( String newID )
    {
        parseSeries(newID);
        updateEntry();
    }

    /*
     * Save this entry to the specified directory.
     * TV episodes have 3 different JSON strings in them, all separated by newlines:
     * 1. seriesJSON - info about the series (including alternatives)
     *      This includes:
     *      active selections
     *      UUID (same as filename)
     * 2. episodeJSON - summary about the active episode
     * 3. seriesDetailsJSON - details about the active series
     * These get written in this order to a single file.
     */
    @SuppressWarnings("unchecked")
    public void save( String basepath, String dirname )
    {
        String filename;
        File dir = new File (dirname);
        if ( !dir.exists() )
        {
            log.error("No such directory: " + dirname);
            return;
        }
        
        /*
         * Generate the VideoLib JSON
         * This is stored in the seriesJSON string.
         */
        try {
            JSONParser parser=new JSONParser();
            JSONObject jsonObject=(JSONObject)parser.parse(seriesJSON);

            /* Get our filename, if we have one. */
            filename = (String)jsonObject.get(VFILENAME);
            if ( filename == null )
            {
                /*
                 * We use a UUID to identify this entry.
                 * We put the UUID in the entry so we always
                 * use the same file no matter which alternative
                 * we choose.
                 */
                filename = new RandomGUID().toString();
                log.info("Video filename: "+ filename);
                jsonObject.put(VFILENAME, filename);
            }

            /*
             * Save our version of the episode title.
             * This should match the name we found for this episode
             * when scanning the directories.
             */
            jsonObject.put(VSERIESID, seriesID);
            jsonObject.put(VEPISODEID, episodeID);
            jsonObject.put(VETITLE, episodeTitle);
            jsonObject.put(VRTITLE, episodeRealTitle);
            jsonObject.put(VEIMAGE, episode_image);
            jsonObject.put(VSIMAGE, series_image);
            jsonObject.put(VOVERVIEW, overview);

            basepath = basepath+File.separator;
            System.out.println("Basepath         : " + basepath);
            System.out.println("Video path before: " + video_path);
            System.out.println("Video path after : " + video_path.replace(basepath, ""));
            jsonObject.put(VVIDEOPATH, video_path.replace(basepath, ""));

            /*
             * Save it back to the seriesJSON.
             */
            seriesJSON = jsonObject.toJSONString();
        }
        catch(Exception e) {
            log.info("Failed to generate VideoLib JSON data.", e);
            return;
        }

        /*
         * Write the JSON strings to file.
         */
        try {
            String path = dirname + File.separator + filename;
            PrintWriter out = new PrintWriter(path);
            for (int i =0; i<3; i++)
            {
                String json = null;

                switch (i)
                {
                    case 0: json = seriesJSON; break;
                    case 1: json = episodeJSON; break;
                    case 2: json = seriesDetailsJSON; break;
                }

                JSONParser parser=new JSONParser();
                JSONObject obj=(JSONObject)parser.parse(json);
                out.println( obj.toJSONString() );
            }
            out.close();
        }
        catch(Exception e) {
            log.info("Failed to save episodeDB JSON data.", e);
        }
    }
}
