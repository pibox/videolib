package com.ximba.videolib;

import java.io.*;
import java.util.*;
import java.net.*;
import org.apache.log4j.Logger;
import org.apache.commons.cli.*;

/* Project Imports */
import com.ximba.common.*;

/**
 * <p> 
 * Command line interface parser.
 * </p>
 * @author Michael J. Hammel
 * @since 1.0
 */

public class Cli {

    /** lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.videolib.Cli");

    // A place to store configuration settings.
    private static final Map<String, String> cfg = new HashMap<String, String>();

    // Directory paths
    private static String runDir       = null;
    private static String runDataDir   = null;
    private static String homeDir      = null;
    private static String videolibDir  = null;

    /* Help strings. */
    private static final String S_OPTION_VERBOSE  = "Enable verbose output";
    private static final String S_OPTION_H        = "Print this message";
    private static final String S_OPTION_VERSION  = "Display version";

    // these are the named properties we use in our configuration mappings.
    private static final String S_VERBOSE         = "verbose";
    private static final String S_VERSION         = "version";
    private static final String S_ENABLED         = "enabled";

    public static final String S_RUNDIR           = "RUNDIR";
    public static final String S_VIDEOLIBDIR      = "VIDEOLIBDIR";
    public static final String S_APIKEY           = "APIKEY";
    // public static final String S_TVAPIKEY         = "TVAPIKEY";
    public static final String S_EXTENSIONS       = "EXTENSIONS";
    public static final String S_MOVIEDIR         = "MOVIEDIR";
    public static final String S_COMPRESSSCRIPT   = "COMPRESSSCRIPT";
    public static final String S_COMPRESSCMD      = "COMPRESSCMD";
    public static final String S_COMPRESSPATH     = "COMPRESSPATH";

    // Prevent enable/disable except from main()
    private boolean inMain = false;

    // The default compression script.
    private static final String defaultScript  = "scripts/fixup.sh";

    // The default compression command.
    private static final String defaultScriptPath  = "/tmp/fixup.sh";

    // The default compression command.
    private static final String defaultCmd  = "/usr/bin/xterm -fa 'Monospace' -fs 14 -e /bin/bash %s %d";

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    public Cli() {}

    /*
     * =======================================================
     * Private methods 
     * =======================================================
     */

    /*
     * Initialize some configuration items, some of which
     * may be overridden by command line options.
     */
    private void init()
    {
        // Don't init more than once.
        if ( runDir != null )
            return;

        runDir    = System.getProperty("user.dir");
        homeDir   = System.getProperty("user.home");
        videolibDir = homeDir + File.separator + ".videolib";

        System.err.println("runDir      : " + runDir);
        System.err.println("homeDir     : " + homeDir);
        System.err.println("videolibDir : " + videolibDir);

        // Create initial directories
        File dir = new File ( videolibDir );
        dir.mkdirs();

        // Initialize our configuration
        cfg.put(S_VERBOSE, "false");
        cfg.put(S_ENABLED, "false");
        cfg.put(S_COMPRESSCMD, defaultCmd);
        cfg.put(S_COMPRESSPATH, defaultScriptPath);

        StringBuilder str = new StringBuilder("");
        BufferedReader br = null;
        try {
            String line;
            br = new BufferedReader(new FileReader(defaultScript));
            while ((line = br.readLine()) != null) {
                str.append(line);
                str.append("\n");
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        cfg.put(S_COMPRESSSCRIPT, str.toString());
    }

    /*
     * =======================================================
     * Public methods 
     * =======================================================
     */

    /** 
      * Parse the command line.
      * @param args Command line arguments.
      */
    void parseCmdLine(String[] args)
    {
        // Don't parse more than once.
        if ( runDir != null )
        {
            log.error("Skipping parsing - runDir already set.");
            return;
        }

        inMain = true;
        init();

        /* Define our boolean options */
        Option o_help1      = new Option( "?", S_OPTION_H );
        Option o_help2      = new Option( "h", false, S_OPTION_H );
        Option o_verbose    = new Option( S_VERBOSE, false, S_OPTION_VERBOSE);
        Option o_version    = new Option( S_VERSION, false, S_OPTION_VERSION);

        /* Define options that take an argument */
        // Option o_gapiv   = new Option( S_GAPIV, true, S_OPTION_GAPIV);

        Options options = new Options();
        options.addOption(o_help1);
        options.addOption(o_help2);
        options.addOption(o_verbose);
        options.addOption(o_version);

        /* create the parser */
        CommandLineParser parser = new GnuParser();
        CommandLine line = null;

        /* parse the command line arguments */
        try {
            line = parser.parse( options, args );
        }
        catch( ParseException exp ) {
            String eMsg = "Parsing failed.  Reason: " + exp.getMessage();
            log.error( eMsg );
            System.err.println( eMsg );
            System.exit(1);
        }

        /* Print the version, if requested. */
        if ( line.hasOption( S_VERSION ) ) {
            System.err.println(BuildInfo.getVersion() );
            System.exit(0);
        }

        /* Set verbose output. */
        if ( line.hasOption( S_VERBOSE ) ) 
            cfg.put(S_VERBOSE, "true");

        /* Print the help message and exit, if requested. */
        if ( (line.hasOption( "h" )) || (line.hasOption( "?" )) ) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "videolib", options );
            System.err.println( "\nCurrent configuration:\n" + 
                                "----------------------\n" + 
                                toString() );
            System.exit(0);
        }
    }

    /*
     * Enable the runtime.
     */
    void enable()
    {
        cfg.put(S_ENABLED, "true");
    }

    /*
     * Disable the runtime.
     */
    void disable()
    {
        cfg.put(S_ENABLED, "false");
    }

    /*
     * Check if we're configured for verbose mode.
     */
    boolean isVerbose()
    {
        String val = cfg.get(S_VERBOSE);
        if ( (val==null) || val.equals("false") )
            return false;
        return true;
    }

    /*
     * Check if we're configured for verbose mode.
     */
    boolean isEnabled()
    {
        String val = cfg.get(S_ENABLED);
        if ( (val==null) || val.equals("false") )
            return false;
        return true;
    }

    /*
     * Save a configuration option.
     */
    public void set( String key, String val )
    {
        if ( val == null )
            return;
        if ( key.equals(S_RUNDIR) || key.equals(S_VIDEOLIBDIR) )
            return;
        log.info("Setting: " + key + " / " + val );
        cfg.put( key, val );
    }

    /*
     * Rest a configuration option to its default value.
     */
    public void reset( String key )
    {
        if ( key == null )
            return;
        if ( key.equals(S_RUNDIR) || key.equals(S_VIDEOLIBDIR) )
            return;

        if ( key.equals(S_COMPRESSSCRIPT) )
        {
            StringBuilder str = new StringBuilder("");
            BufferedReader br = null;
            try {
                String line;
                br = new BufferedReader(new FileReader(defaultScript));
                while ((line = br.readLine()) != null) {
                    str.append(line);
                    str.append("\n");
                }
            } 
            catch (IOException e) {
                e.printStackTrace();
            } 
            finally {
                try {
                    if (br != null)
                        br.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            cfg.put(S_COMPRESSSCRIPT, str.toString());
            return;
        }

        if ( key.equals(S_COMPRESSCMD) )
        {
            cfg.put(S_COMPRESSCMD, defaultCmd);
            return;
        }

        if ( key.equals(S_COMPRESSPATH) )
        {
            cfg.put(S_COMPRESSPATH, defaultScriptPath);
            return;
        }
    }

    /*
     * Retrieve a configuration option.
     */
    String get( String val )
    {
        if ( val == null )
            return null;

        // Immutable settings
        if ( val.equals(S_RUNDIR) )
            return runDir;
        if ( val.equals(S_VIDEOLIBDIR) )
            return videolibDir;

        log.info("wanted: " + val + ", got " + cfg.get(val));
        return cfg.get( val );
    }

    /*
     * Convert the name/value pairs to a printable string.
     */
    public String toString()
    {
        StringBuilder str = new StringBuilder("");

        // Immutables go first.
        str.append( "VideoLib directory: " + videolibDir + "\n");

        // Then grab configurable items.
        for (Map.Entry<String, String> entry : cfg.entrySet())
            str.append( entry.getKey() + ": " + entry.getValue() + "\n");
        return str.toString();
    }

    /*
     * Get configuration data.
     */
    @SuppressWarnings("unchecked")
    public void load()
    {
        String filename = get(Cli.S_VIDEOLIBDIR) + File.separator + "config";
        File file = new File( filename );
        if ( !file.exists() )
        {
            log.warn("Configuration not read - no such file.");
            return;
        }

        log.info("Reading saved configuration.");
        Map<String, String> newCfg = null;
        try {
            FileInputStream fin = new FileInputStream(filename);
            ObjectInputStream ois = new ObjectInputStream(fin);
            newCfg = (HashMap<String,String>) ois.readObject();
            ois.close();
        }
        catch(Exception e){
            log.error("Failed to save configuration.", e);
        }
        if ( newCfg == null )
        {
            log.error("Configuration file exists but no data found.");
            return;
        }

        // Merge with current settings.
        Iterator it = newCfg.entrySet().iterator();
        while ( it.hasNext() ) 
        {
            Map.Entry pairs = (Map.Entry)it.next();
            log.info("key/value: " + (String)pairs.getKey() + " / " + (String)pairs.getValue());
            cfg.put( (String)pairs.getKey(), (String)pairs.getValue() );
        }
    }

    /*
     * Save configuration data.
     */
    public void save()
    {
        try {
            String filename = get(Cli.S_VIDEOLIBDIR) + File.separator + "config";
            FileOutputStream fout = new FileOutputStream( filename );
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(cfg);
            oos.close();
        }
        catch(Exception e){
            log.error("Failed to save configuration.", e);
        }
    }
}
