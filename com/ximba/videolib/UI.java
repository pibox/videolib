package com.ximba.videolib;

// Java
import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

// SWT
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;

// VideoLib
import com.ximba.common.*;

/**
 * <p> 
 * Front end to UI thread.
 * </p>
 * @author Michael J. Hammel
 * @since 1.0
 */

public class UI implements Runnable {

    /* lo4j logger */
    private static final Logger log = Logger.getLogger("com.ximba.videolib.UI");

    /*
     * --------------------------------------------
     * Thread Managment
     * --------------------------------------------
     */

    /* This thread. */
    private Thread thread = null;

    /* When true, the thread is running. */
    private boolean isRunning = false;

    /* When true, the thread has completed and is ready to change state to "not running". */
    private boolean runComplete = false;

    /* When shutting down, disable exception hanlding in run() method. */
    private boolean doShutdown = false;

    /*
     * --------------------------------------------
     * Fonts and Colors
     * --------------------------------------------
     */
    Font  boldFont       = null;
    Font  mediumFont     = null;
    Font  disclaimerFont = null;
    Color red            = null;

    /*
     * --------------------------------------------
     * Numerics
     * --------------------------------------------
     */

    private static final int DIRBUTTON       = 0;
    private static final int SCANNEWBUTTON   = 1;
    private static final int RESCANBUTTON    = 2;
    private static final int COMPRESSBUTTON  = 3;
    private static final int MAX_BUTTONS     = COMPRESSBUTTON + 1;

    private static final int MOVIE_TAB       = 0;
    private static final int TV_TAB          = 1;
    
    /*
     * --------------------------------------------
     * Strings
     * --------------------------------------------
     */

    // The list of extensions supported by this application.
    public static final String[] extensions = {
        ".mp4",
        ".mkv",
        ".avi"
    };

    // Disclaimer
    private static final String disclaimer = 
        "VideoLib uses, but is not endorsed or certified by, the TheMovieDB.org API.";

    // "About" message
    private static final String about = 
        "A video library generator that utilizes TheMovieDB.org.\nPart of the PiBox Media System.";

    // The upstream site logo images
    private static final String gfxmuseLogoFile  = "images/gfxmuse-logo.png";
    private static final String tmdbLogoFile     = "images/tmdb-logo.png";

    /*
     * --------------------------------------------
     * UI Components
     * --------------------------------------------
     */
    Display display = null;
    Shell shell = null;
    org.eclipse.swt.widgets.List movieList = null;
    Text dirName = null;

    Button[] buttons = new Button[MAX_BUTTONS];

    Text releaseDate = null;
    Text tmdbName = null;
    Text videoName = null;
    Text overview = null;
    Table alternativesTable = null;
    Label posterLabel = null;
    Label seriesLabel = null;
    Label episodeLabel = null;
    Text apiText = null;
    static Text extensionsText = null;
    Menu posterMenu = null;
    MenuItem[] posterItem = new MenuItem[9];
    String scanType = "movie";

    /*
     * A place for images.
     */
    ViewForm viewForm = null;
    Control[] viewFormContent = new Control[2];

    // List of files
    java.util.List<File> fileListing = null;

    // List of Movies and TV episodes
    java.util.List<MovieDB> movies = new ArrayList<MovieDB>();
    java.util.List<TVDB> episodes = new ArrayList<TVDB>();

    /* Menus indices */
    private static final int M_FILE_SAVE            = 0;
    private static final int M_FILE_QUIT            = 1;
    private static final int M_HELP_ABOUT           = 0;

    /* Config indices */
    private static final int M_CONFIG_COMPRESSION   = 0;

    /*
     * Menus in Menu Bar
     */
    private static String[] FileMenuItems = {
        "&Save",
        "&Quit"
    };
    private static String[] ConfigMenuItems = {
        "&Compression",
    };
    private static String[] HelpMenuItems = {
        "&About",
    };

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    void UI() { }

    /*
     * =======================================================
     * Inner classes
     * =======================================================
     */

    /*
     * Runs movie queries to upstream provided as a background
     * task.
     */
    private class ScanMovies extends TimerTask
    {
        boolean rescan = false;
        public ScanMovies(boolean doScan) {
            this.rescan = doScan;
        }

        public void run() {
            Display.getDefault().asyncExec( new Runnable() {
                public void run() {
                    for(int i=0; i<MAX_BUTTONS; i++)
                    {
                        buttons[i].setEnabled(false);
                    }
                }
            });
            String firstTitle = null;
            Iterator mit = movies.iterator();
            boolean perform = false;
            while( mit.hasNext() )
            {
                MovieDB movie = (MovieDB)mit.next();
                if ( firstTitle == null )
                    firstTitle = movie.getTitle();

                if ( rescan )
                {
                    perform = true;
                }
                else
                {
                    if (movie.getOverview() == null )
                    {
                        perform = true;
                    }
                }

                if ( perform )
                {
                    try {
                        movie.queryDB();
                        Display.getDefault().asyncExec( new Runnable() {
                            public void run() {
                                setSelectedMovie( movie.getTitle() );
                                updateDetails( movie.getTitle() );
                            }
                        });
                    }
                    catch(Exception e) {
                        log.info("GeneralException: Movie query failed", e);
                        e.printStackTrace();
                    }
                    try { Thread.sleep(350); }
                    catch(InterruptedException ie) { }
                }
                perform = false;
            }
            final String title = firstTitle;
            Display.getDefault().asyncExec( new Runnable() {
                public void run() {
                    setSelectedMovie( title );
                    updateDetails( title );
                    for(int i=0; i<MAX_BUTTONS; i++)
                    {
                        buttons[i].setEnabled(true);
                    }
                }
            });
        }
    }

    /*
     * Runs TV queries to upstream provided as a background
     * task.
     */
    private class ScanTV extends TimerTask
    {
        boolean rescan = false;
        public ScanTV(boolean doScan) {
            this.rescan = doScan;
        }

        public void run() {
            Display.getDefault().asyncExec( new Runnable() {
                public void run() {
                    for(int i=0; i<MAX_BUTTONS; i++)
                    {
                        buttons[i].setEnabled(false);
                    }
                }
            });
            String firstTitle = null;
            Iterator mit = episodes.iterator();
            boolean perform = false;
            while( mit.hasNext() )
            {
                TVDB episode = (TVDB)mit.next();
                if ( firstTitle == null )
                    firstTitle = episode.getTitle();

                if ( rescan )
                {
                    perform = true;
                }
                else
                {
                    if (episode.getOverview() == null )
                    {
                        perform = true;
                    }
                }

                if ( perform )
                {
                    try {
                        episode.queryDB();
                        Display.getDefault().asyncExec( new Runnable() {
                            public void run() {
                                setSelectedMovie( episode.getTitle() );
                                updateDetailsTV( episode.getTitle() );
                            }
                        });
                    }
                    catch(Exception e) {
                        log.info("GeneralException: TVDB query failed", e);
                        e.printStackTrace();
                    }
                    try { Thread.sleep(350); }
                    catch(InterruptedException ie) { }
                }
                perform = false;
            }
            final String title = firstTitle;
            Display.getDefault().asyncExec( new Runnable() {
                public void run() {
                    setSelectedMovie( title );
                    updateDetailsTV( title );
                    for(int i=0; i<MAX_BUTTONS; i++)
                    {
                        buttons[i].setEnabled(true);
                    }
                }
            });
        }
    }

    /*
     * =======================================================
     * Private UI methods
     * =======================================================
     */

    // Find video based on current selection in movie list.
    private MovieDB findMovie(String title)
    {
        Iterator it = movies.iterator();
        while( it.hasNext() )
        {
            MovieDB movie = (MovieDB)it.next();
            if ( (movie.getTitle()!=null) && movie.getTitle().equals(title) )
            {
                return movie;
            }
        }
        return null;
    }

    // Find video based on current selection in tv list.
    private TVDB findTVDB(String title)
    {
        log.info("Looking for " + title);
        Iterator it = episodes.iterator();
        while( it.hasNext() )
        {
            TVDB tvdb = (TVDB)it.next();
            log.info("Testing " + tvdb.getTitle());
            if ( (tvdb.getTitle()!=null) && tvdb.getTitle().equals(title) )
            {
                return tvdb;
            }
            log.info("Testing " + tvdb.getRealTitle());
            if ( (tvdb.getRealTitle()!=null) && tvdb.getRealTitle().equals(title) )
            {
                return tvdb;
            }
        }
        return null;
    }

    // Find video based on current selection in movie list.
    private void setSelectedMovie(String title)
    {
        String[] entries = movieList.getItems();
        for ( int idx=0; idx<entries.length; idx++ )
        {
            if ( entries[idx].equals(title) )
            {
                movieList.setSelection(idx);
                return;
            }
        }
    }

    /*
     * Update the details and poster based on a video entry.
     */
    private void updateDetails( String title )
    {
        clearImage();
        releaseDate.setText( "" );
        overview.setText( "" );
        tmdbName.setText( "" );

        log.info("Title: " + title );
        MovieDB movie = findMovie(title);
        if ( movie == null )
        {
            log.error("No matching entry for " + title);
            return;
        }

        /* Update relate date */
        if ( movie.getReleaseDate() != null )
        {
            log.info("Release date: " + movie.getReleaseDate() );
            releaseDate.setText( movie.getReleaseDate() );
        }

        /* Update overview text */
        if ( movie.getOverview() != null )
        {
            log.info("Overview: " + movie.getOverview() );
            try {
                overview.setText( URLDecoder.decode(movie.getOverview(), "UTF-8") );
            }
            catch(Exception e) {
                log.warn("Failed to decode overview.");
                overview.setText( movie.getOverview() );
            }
        }

        /* Set movie title */
        tmdbName.setText( title );

        /* Update movie poster */
        if ( movie.getPosterPath() != null )
        {
            String path = dirName.getText() + File.separator + 
                "posters" + File.separator + movie.getPosterPath();
            File file = new File(path);
            if ( file.exists() )
            {
                Image image = new Image(shell.getDisplay(), path);
                Rectangle size = posterLabel.getBounds();
                log.info( "Poster size: " + size);
                Image posterImage = new Image(shell.getDisplay(), 
                        image.getImageData().scaledTo(size.width,size.height));
                posterLabel.setImage(posterImage);
            }
            else
                log.warn("Can't find poster: " + path);
        }

        /* Update alternatives to table */
        alternativesTable.setRedraw(false);
        alternativesTable.removeAll();
        java.util.List<String> altList = movie.getAlternatives();
        if ( altList != null )
        {
            Iterator it = altList.iterator();
            while ( it.hasNext() )
            {
                String data = (String)it.next();
                log.info("Alternative: " + data);
                String field[] = data.split(":");
                String altID = field[0];
                String altDate = field[1];
                String altTitle = data.replace(altID+":"+altDate+":","");
                TableItem item = new TableItem(alternativesTable, SWT.NONE);
                item.setText(new String[] { altDate, altTitle, altID });
                item.setFont(0, mediumFont);
                item.setFont(1, mediumFont);
            }
        }
        alternativesTable.setRedraw(true);
    }

    /*
     * Update the details and poster based on a TV episode entry.
     */
    private void updateDetailsTV( String title )
    {
        clearImage();
        releaseDate.setText( "" );
        overview.setText( "" );
        tmdbName.setText( "" );

        log.info("Title: " + title );
        TVDB episode = findTVDB(title);
        if ( episode == null )
        {
            log.error("No matching entry for " + title);
            return;
        }

        /* Update relate date */
        if ( episode.getReleaseDate() != null )
        {
            log.info("Release date: " + episode.getReleaseDate() );
            releaseDate.setText( episode.getReleaseDate() );
        }

        /* Update overview text */
        if ( episode.getOverview() != null )
        {
            log.info("Overview: " + episode.getOverview() );
            try {
                overview.setText( URLDecoder.decode(episode.getOverview(), "UTF-8") );
            }
            catch(Exception e) {
                log.warn("Failed to decode overview.");
                overview.setText( episode.getOverview() );
            }
        }

        /* Set episode title */
        tmdbName.setText( title );
        if ( episode.getRealTitle() != null )
            videoName.setText( episode.getRealTitle() );

        /* Update episode poster */
        if ( episode.getEpisodeImage() != null )
        {
            String path = dirName.getText() + File.separator + 
                "posters" + File.separator + episode.getEpisodeImage();
            log.info("episode image path: " + path);
            File file = new File(path);
            if ( file.exists() )
            {
                Image image = new Image(shell.getDisplay(), path);
                Rectangle size = episodeLabel.getBounds();
                log.info( "Poster size: " + size);
                Image posterImage = new Image(shell.getDisplay(), 
                        image.getImageData().scaledTo(size.width,size.height));
                episodeLabel.setImage(posterImage);
            }
            else
                log.warn("Can't find episode poster: " + path);
        }
        else
            log.warn("No episode image available");

        /* Update series poster */
        if ( episode.getSeriesImage() != null )
        {
            String path = dirName.getText() + File.separator + 
                "posters" + File.separator + episode.getSeriesImage();
            File file = new File(path);
            if ( file.exists() )
            {
                try {
                    Image image = new Image(shell.getDisplay(), path);
                    Rectangle size = seriesLabel.getBounds();
                    log.info( "Poster size: " + size);
                    Image posterImage = new Image(shell.getDisplay(), 
                        image.getImageData().scaledTo(size.width,size.height));
                    seriesLabel.setImage(posterImage);
                }
                catch(Exception e)
                {
                    log.warn("Failed to get poster: " + path, e);
                }
            }
            else
                log.warn("Can't find series poster: " + path);
        }

        /* Update alternatives to table */
        alternativesTable.setRedraw(false);
        alternativesTable.removeAll();
        java.util.List<String> altList = episode.getAlternatives();
        if ( altList != null )
        {
            Iterator it = altList.iterator();
            while ( it.hasNext() )
            {
                String data = (String)it.next();
                log.info("Alternative: " + data);
                String field[] = data.split(":");
                String altID = field[0];
                String altDate = field[1];
                String altTitle = data.replace(altID+":"+altDate+":","");
                TableItem item = new TableItem(alternativesTable, SWT.NONE);
                item.setText(new String[] { altDate, altTitle, altID });
                item.setFont(0, mediumFont);
                item.setFont(1, mediumFont);
            }
        }
        alternativesTable.setRedraw(true);
    }

    private void clearImage()
    {
        int     width, height;

        if ( scanType.equals("movie" ) )
        {
            width = 400;
            height = 800;
        }
        else
        {
            width = 400;
            height = 400;
        }

        Image image = new Image(shell.getDisplay(), width,height);
        GC gc = new GC(image);
        Color white = Display.getDefault().getSystemColor(SWT.COLOR_WHITE);
        gc.setForeground(white);
        gc.drawRectangle(0,0,width,height);
        Color black = Display.getDefault().getSystemColor(SWT.COLOR_BLACK);
        gc.setForeground(black);
        gc.drawText("No image available",0,0);
        gc.dispose();

        if ( scanType.equals("movie" ) )
        {
            posterLabel.setImage(image);
        }
        else
        {
            seriesLabel.setImage(image);
            episodeLabel.setImage(image);
        }
    }

    /*
     * File menu handler
     */
    private void fileMenuHandler( int idx ) 
    {
        switch(idx)
        {
            case M_FILE_QUIT:   
                Cli cli = new Cli();
                cli.disable();
                break;

            case M_FILE_SAVE:   
                // Test for old .videlog *file* and remove if requested.
                String filename = dirName.getText() + File.separator + ".videolib";
                File file = new File (filename);
                if ( file.exists() )
                {
                    int style = SWT.APPLICATION_MODAL | SWT.ICON_QUESTION | SWT.YES | SWT.NO;
                    MessageBox msgBox = new MessageBox(shell, style);
                    msgBox.setText("VideoLib flat file");
                    msgBox.setMessage("The old .videolib file exists here.\nWould you like to remove it?");
                    if ( msgBox.open() == SWT.YES )
                    {
                        log.info("Removing old .videolib");
                        file.delete();
                    }
                }

                /*
                 * Create .videolib directory in the same place where we
                 * scanned for movies.
                 */
                String dirname = dirName.getText() + File.separator + "videolib";
                File dir = new File ( dirname );
                dir.mkdirs();

                /* 
                 * Iterate over all movies and store their JSON to the 
                 * .videolib directory.
                 */
                if ( scanType.equals("movie" ) )
                {
                    dir = new File ( dirname + File.separator + "movies" );
                    dir.mkdirs();
                    for(File file2: dir.listFiles())
                    {
                        if (file2.isFile())
                        {
                            file2.delete();
                        }
                    }
                    Iterator mit = movies.iterator();
                    while( mit.hasNext() )
                    {
                        MovieDB movie = (MovieDB)mit.next();
                        movie.save(dirName.getText(), dirname + File.separator + "movies");
                    }
                }
                else
                {
                    dir = new File ( dirname + File.separator + "tv" );
                    dir.mkdirs();
                    for(File file2: dir.listFiles())
                    {
                        if (file2.isFile())
                        {
                            file2.delete();
                        }
                    }
                    Iterator mit = episodes.iterator();
                    while( mit.hasNext() )
                    {
                        TVDB episode = (TVDB)mit.next();
                        episode.save(dirName.getText(), dirname + File.separator + "tv");
                    }
                }
                break;
        }
    }

    /*
     * Config menu handler
     */
    private void configMenuHandler( int idx ) 
    {
        switch(idx)
        {
            case M_CONFIG_COMPRESSION:   
                Shell configShell = new Shell(shell, SWT.APPLICATION_MODAL);
                configShell.setSize(500,500);
                configShell.setText("Compression Script");

                GridLayout gl = new GridLayout();
                gl.marginWidth = 0;
                gl.marginHeight = 0;
                gl.verticalSpacing = 0;
                gl.horizontalSpacing = 0;
                gl.numColumns = 1;
                gl.makeColumnsEqualWidth = false;
                configShell.setLayout(gl);

                GridData gd = new GridData(GridData.FILL_HORIZONTAL);
                Composite composite = new Composite(configShell, SWT.NONE);
                gl = new GridLayout();
                gl.marginWidth = 5;
                gl.marginHeight = 4;
                gl.verticalSpacing = 10;
                gl.horizontalSpacing = 0;
                gl.numColumns = 1;
                gl.makeColumnsEqualWidth = false;
                composite.setLayout(gl);
                composite.setLayoutData(gd);

                gd = new GridData(GridData.FILL_HORIZONTAL);
                Label label = new Label( composite, SWT.NONE );
                label.setText( "Command: %s is the script, %d is the poster directory." );
                label.setLayoutData(gd);

                gd = new GridData(GridData.FILL_HORIZONTAL);
                Text command = new Text(composite, SWT.BORDER );
                command.setLayoutData(gd);

                Cli cli = new Cli();
                String script = cli.get(Cli.S_COMPRESSCMD);
                if ( script != null )
                {
                    command.setText(script);
                }

                gd = new GridData(GridData.FILL_BOTH);
                gd.widthHint = 500;
                gd.heightHint = 500;
                Text text = new Text(configShell, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
                text.setBounds(10, 10, 100, 100);
                text.setLayoutData(gd);
                script = cli.get(Cli.S_COMPRESSSCRIPT);
                if ( script != null )
                {
                    text.setText(script);
                }

                composite = new Composite(configShell, SWT.NONE);
                gl = new GridLayout();
                gl.marginWidth = 0;
                gl.marginHeight = 0;
                gl.verticalSpacing = 10;
                gl.horizontalSpacing = 0;
                gl.numColumns = 3;
                gl.makeColumnsEqualWidth = false;
                composite.setLayout(gl);

                Button button = new Button(composite, SWT.PUSH);
                button.setText("Close");
                button.addSelectionListener(new SelectionListener() {
                    public void widgetSelected(SelectionEvent ev) {
                        configShell.dispose();
                    }
                    public void widgetDefaultSelected(SelectionEvent ev) {}
                });

                button = new Button(composite, SWT.PUSH);
                button.setText("Save");
                button.addSelectionListener(new SelectionListener() {
                    public void widgetSelected(SelectionEvent ev) {
                        cli.set(Cli.S_COMPRESSSCRIPT, text.getText());
                        cli.set(Cli.S_COMPRESSCMD, command.getText());
                        cli.save();
                    }
                    public void widgetDefaultSelected(SelectionEvent ev) {}
                });

                button = new Button(composite, SWT.PUSH);
                button.setText("Reset");
                button.addSelectionListener(new SelectionListener() {
                    public void widgetSelected(SelectionEvent ev) {
                        cli.reset(Cli.S_COMPRESSSCRIPT);
                        cli.reset(Cli.S_COMPRESSCMD);
                        String script = cli.get(Cli.S_COMPRESSSCRIPT);
                        text.setText(script);
                        script = cli.get(Cli.S_COMPRESSCMD);
                        command.setText(script);
                    }
                    public void widgetDefaultSelected(SelectionEvent ev) {}
                });

                configShell.open();
                while ( isRunning && !configShell.isDisposed() )
                {
                    if ( !display.readAndDispatch() )
                        display.sleep();
                }
                if ( !configShell.isDisposed() )
                    configShell.dispose();
                break;
        }
    }

    /*
     * Help menu handler
     */
    private void helpMenuHandler( int idx ) 
    {
        switch(idx)
        {
            case M_HELP_ABOUT:   
                Shell aboutShell = new Shell(shell, SWT.APPLICATION_MODAL);
                aboutShell.setSize(500,365);
                aboutShell.setText("About VideoLib");

                GridLayout gl = new GridLayout();
                gl.marginWidth = 0;
                gl.marginHeight = 0;
                gl.verticalSpacing = 0;
                gl.horizontalSpacing = 0;
                gl.numColumns = 1;
                gl.makeColumnsEqualWidth = false;
                aboutShell.setLayout(gl);
                aboutShell.setBackground(display.getSystemColor(SWT.COLOR_WHITE));

                // "About" text
                Composite composite = new Composite(aboutShell, SWT.BORDER);
                GridData gd = new GridData(GridData.FILL_HORIZONTAL);
                gl = new GridLayout();
                gl.marginWidth = 5;
                gl.marginHeight = 4;
                gl.verticalSpacing = 15;
                gl.horizontalSpacing = 0;
                gl.numColumns = 1;
                gl.makeColumnsEqualWidth = false;
                composite.setLayout(gl);
                composite.setLayoutData(gd);
                composite.setBackground( aboutShell.getBackground() );

                // VideoLib Logo
                Label label = new Label(composite, SWT.NONE);
                Image gfxmuseLogo = new Image(shell.getDisplay(), gfxmuseLogoFile);
                label.setImage(gfxmuseLogo);
                gd = new GridData(GridData.FILL_HORIZONTAL);
                gd.grabExcessVerticalSpace = true;
                label.setLayoutData(gd);
                label.setBackground( aboutShell.getBackground() );

                gd = new GridData(GridData.FILL_HORIZONTAL);
                label = new Label( composite, SWT.NONE );
                label.setText( about );
                label.setLayoutData(gd);
                label.setBackground( aboutShell.getBackground() );
                label.setAlignment( SWT.CENTER );

                // Disclaimer and logo
                label = new Label(composite, SWT.NONE);
                Image tmdbLogo = new Image(shell.getDisplay(), tmdbLogoFile);
                label.setImage(tmdbLogo);
                gd = new GridData(GridData.FILL_HORIZONTAL);
                label.setLayoutData(gd);
                label.setBackground( aboutShell.getBackground() );

                Text text = new Text(composite, SWT.MULTI|SWT.WRAP|SWT.READ_ONLY|SWT.CENTER);
                text.setText(disclaimer);
                text.setFont(disclaimerFont);
                gd = new GridData(GridData.FILL_HORIZONTAL);
                gd.widthHint = 320;
                gd.grabExcessVerticalSpace = true;
                text.setLayoutData(gd);
                text.setBackground( aboutShell.getBackground() );

                // Buttons
                composite = new Composite(aboutShell, SWT.NONE);
                gl = new GridLayout();
                gl.marginWidth = 0;
                gl.marginHeight = 0;
                gl.verticalSpacing = 10;
                gl.horizontalSpacing = 0;
                gl.numColumns = 3;
                gl.makeColumnsEqualWidth = false;
                composite.setLayout(gl);
                composite.setBackground( aboutShell.getBackground() );

                Button button = new Button(composite, SWT.PUSH);
                button.setText("Close");
                button.setBackground( aboutShell.getBackground() );

                button.addSelectionListener(new SelectionListener() {
                    public void widgetSelected(SelectionEvent ev) {
                        aboutShell.dispose();
                    }
                    public void widgetDefaultSelected(SelectionEvent ev) {}
                });

                aboutShell.open();
                while ( isRunning && !aboutShell.isDisposed() )
                {
                    if ( !display.readAndDispatch() )
                        display.sleep();
                }
                if ( !aboutShell.isDisposed() )
                    aboutShell.dispose();
                break;
        }
    }

    /* Load saved entries from specified directory. */
    private void loadDirectory (File dir) throws FileNotFoundException {
        File[] files = dir.listFiles();
        java.util.List<File> fileList = Arrays.asList(files);
        for(File file : fileList) {
            if ( file.isFile() )
            {
                String name = file.getName();
                String path = dir.getPath() + File.separator + name;
                try {
                    if ( scanType.equals("movie" ) )
                    {
                        /*
                         * We'll load the path from the JSON data.
                         */
                        MovieDB movie = new MovieDB( path );
                        movies.add(movie);
                    }
                    else
                    {
                        TVDB episode = new TVDB( path );
                        episodes.add(episode);
                    }
                }
                catch(Exception e) {
                    log.error("Error loading file " + path, e);
                }
            }
        }
    }

    /* Get a file listing in a directory */
    private java.util.List<File> getFileListing (File currentDir) throws FileNotFoundException {
        String[] exts = extensionsText.getText().split(" ");
        java.util.List<File> result = new ArrayList<>();
        File[] filesAndDirs = currentDir.listFiles();
        java.util.List<File> filesDirs = Arrays.asList(filesAndDirs);
        for(File file : filesDirs) {
            String name = file.getName();
            for(int i=0; i<exts.length; i++)
            {
                if ( name.endsWith(exts[i]) )
                    result.add(file); //Only add video files
            }
            if (! file.isFile()) {
                //must be a directory
                //recursive call!
                java.util.List<File> deeperList = getFileListing (file);
                result.addAll(deeperList);
            }
        }
        return result;
    }

    /** Choose a directory */
    private String selectDir()
    {
        DirectoryDialog dlg = new DirectoryDialog(shell, SWT.OPEN);
        dlg.setMessage("Choose a directory");

        File file = new File(dirName.getText());
        if ( file.exists() )
            dlg.setFilterPath( dirName.getText());
        else
            dlg.setFilterPath( System.getenv("user.home") );

        String filename = dlg.open();
        if ( filename == null )
            return null;
        return filename;
    }

    /** Rescan based on directory listing */
    private void scanByFilename()
    {
        /* Generate a file listing and sort it */
        File currentDir = new File( dirName.getText() );
        try { fileListing = getFileListing (currentDir); }
        catch(Exception e) {
            log.error("Failed to get file list.", e);
            return;
        }
        Collections.sort(fileListing);

        if ( scanType.equals("movie" ) )
        {
            addMovies();
        }
        else
        {
            addTV();
        }

        String[] entries = movieList.getItems();
        setSelectedMovie( entries[0] );
        if ( scanType.equals("movie" ) )
        {
            updateDetails( entries[0] );
        }
        else
        {
            updateDetailsTV( entries[0] );
        }
    }

    /* We're working with movies, so add those here. */
    private void addMovies()
    {
        /* 
         * Add new movies to our list of MovieDB entries.
         */
        Iterator it = fileListing.iterator();
        while( it.hasNext() )
        {
            File entry = (File)it.next();
            log.info("addMovies: File: " + entry.getPath());

            String name = entry.getName();
            String[] exts = extensionsText.getText().split(" ");
            for(int i=0; i<exts.length; i++)
            {
                name = name.replace("."+exts[i], "");
            }
            name = name.replace("_", " ");
            movieList.add( name );

            /* Add it to our entries, if we don't already have one. */
            MovieDB movie = findMovie(name);
            if ( movie == null )
            {
                try {
                    movie = new MovieDB(name);
                    movies.add(movie);
                    movie.setPath(entry.getPath());
                }
                catch(Exception e) {
                    log.error("Error trying to add new movie entry.");
                }
            }
        }
    }

    /* We're working with TV episodes, so add those here. */
    private void addTV()
    {
        /* 
         * Add new movies to our list of MovieDB entries.
         */
        Iterator it = fileListing.iterator();
        while( it.hasNext() )
        {
            File entry = (File)it.next();
            log.info("addTV: File: " + entry.getPath());

            String name = entry.getName();
            String[] exts = extensionsText.getText().split(" ");
            for(int i=0; i<exts.length; i++)
            {
                name = name.replace("."+exts[i], "");
            }
            name = name.replace("_", " ");
            movieList.add( name );

            /* Add it to our entries, if we don't already have one. */
            TVDB episode = findTVDB(name);
            final String title = name;
            if ( episode == null )
            {
                try {
                    episode = new TVDB(title);
                    episodes.add(episode);
                    episode.setPath(entry.getPath());
                }
                catch(Exception e) {
                    log.error("Error trying to add new TV episode.");
                }
            }
        }
    }

    /* List of movies */
    private void buildListFrame() 
    {
        GridData gd = new GridData(GridData.FILL_BOTH);
        Composite composite = new Composite(shell, SWT.NONE);
        composite.setLayout(new GridLayout(1, false));
        composite.setLayoutData(gd);

        // TheMovieDB.org API Key
        gd = new GridData(GridData.FILL_HORIZONTAL);
        Group group = new Group(composite, SWT.NONE);
        group.setText("TheMovieDB API Key");
        group.setFont(boldFont);
        group.setLayoutData(gd);

        GridLayout gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 2;
        gl.makeColumnsEqualWidth = false;
        group.setLayout(gl);

        gd = new GridData(GridData.FILL_HORIZONTAL);
        apiText = new Text(group, SWT.BORDER);
        apiText.setLayoutData(gd);

        Button button = new Button(group, SWT.PUSH);
        button.setText("Save");
        button.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent ev) {
                Cli cli = new Cli();
                cli.load();
                cli.set(Cli.S_APIKEY, apiText.getText());
                cli.save();
            }
            public void widgetDefaultSelected(SelectionEvent ev) {}
        });

        // Filename extensions
        gd = new GridData(GridData.FILL_HORIZONTAL);
        group = new Group(composite, SWT.NONE);
        group.setText("File extensions");
        group.setFont(boldFont);
        group.setLayoutData(gd);

        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 2;
        gl.makeColumnsEqualWidth = false;
        group.setLayout(gl);

        gd = new GridData(GridData.FILL_HORIZONTAL);
        extensionsText = new Text(group, SWT.BORDER);
        extensionsText.setLayoutData(gd);
        Cli cli = new Cli();
        String value = cli.get(Cli.S_EXTENSIONS);
        if ( value != null )
        {
            log.info("Extensions read from properties: " + value);
            value = value.replace( ",", " " );
            value = value.trim().replaceAll( " +", " " );
            extensionsText.setText(value);
        }
        else
        {
            StringBuilder str = new StringBuilder();
            String sep = "";
            for(int i=0; i<extensions.length; i++)
            {
                str.append( sep );
                str.append( extensions[i].replace(".", "") );
                sep = " ";
            }
            extensionsText.setText( str.toString() );
        }

        button = new Button(group, SWT.PUSH);
        button.setText("Save");
        button.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent ev) {
                Cli cli = new Cli();
                cli.load();
                cli.set(Cli.S_EXTENSIONS, extensionsText.getText());
                cli.save();
            }
            public void widgetDefaultSelected(SelectionEvent ev) {}
        });

        // Database selection
        gd = new GridData(GridData.FILL_HORIZONTAL);
        group = new Group(composite, SWT.NONE);
        group.setText("Database");
        group.setFont(boldFont);
        group.setLayoutData(gd);

        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 2;
        gl.makeColumnsEqualWidth = true;
        group.setLayout(gl);

        button = new Button(group, SWT.RADIO);
        button.setText("Movies");
        gd = new GridData(GridData.FILL_HORIZONTAL);
        button.setLayoutData(gd);
        button.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent ev) {
                scanType = "movie";
                viewForm.setContent( viewFormContent[MOVIE_TAB] );
            }
            public void widgetDefaultSelected(SelectionEvent ev) {}
        });
        button.setSelection(true);
        button = new Button(group, SWT.RADIO);
        button.setText("TV");
        gd = new GridData(GridData.FILL_HORIZONTAL);
        button.setLayoutData(gd);
        button.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent ev) {
                scanType = "tv";
                viewForm.setContent( viewFormContent[TV_TAB] );
            }
            public void widgetDefaultSelected(SelectionEvent ev) {}
        });

        // Movie List
        gd = new GridData(GridData.FILL_BOTH);
        gd.grabExcessVerticalSpace = true;
        group = new Group(composite, SWT.NONE);
        group.setText("Movie List");
        group.setFont(boldFont);
        group.setLayoutData(gd);

        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 3;
        gl.makeColumnsEqualWidth = false;
        group.setLayout(gl);

        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.widthHint = 200;
        gd.horizontalSpan = 2;
        dirName = new Text(group, SWT.BORDER);
        dirName.setLayoutData(gd);

        /*
         * Once a directory is selected any existing database is loaded.
         * Then a file listing of all movie files is created.
         * Finally, any movies in the file listing that were not in the database
         * are added to the database.  However, the new movies have not been
         * scanned yet.
         */
        buttons[DIRBUTTON] = new Button(group, SWT.PUSH);
        buttons[DIRBUTTON].setText("Select");
        buttons[DIRBUTTON].addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent ev) {
                String dir = selectDir();
                if ( dir == null )
                    return;
                log.info("Dirname = " + dir);
                File file = new File( dir );
                if ( !file.exists() || !file.isDirectory() )
                {
                    log.error(dir + " does not exist.");
                    return;
                }
                dirName.setText(dir);
                Cli cli = new Cli();
                cli.set(Cli.S_MOVIEDIR, dir);
                cli.save();

                movieList.setRedraw(false);
                movieList.removeAll();
                movieList.setRedraw(true);
                movies.clear();
                episodes.clear();

                /* If there is an existing database, load it. */
                String dirname;
                if ( scanType.equals("movie" ) )
                    dirname = dirName.getText() + File.separator + "videolib" + File.separator + "movies";
                else
                    dirname = dirName.getText() + File.separator + "videolib" + File.separator + "tv";
                File olddir = new File ( dirname );
                if ( olddir.exists() )
                {
                    try { loadDirectory(olddir); }
                    catch(Exception e){
                        log.error("Error trying read existing db.", e);
                    }
                }

                scanByFilename();

                /* Generate a file listing and sort it */
                /*
                File currentDir = new File( dirName.getText() );
                try { fileListing = getFileListing (currentDir); }
                catch(Exception e) {
                    log.error("Failed to get file list.", e);
                    return;
                }
                Collections.sort(fileListing);

                if ( scanType.equals("movie" ) )
                {
                    addMovies();
                }
                else
                {
                    addTV();
                }

                String[] entries = movieList.getItems();
                setSelectedMovie( entries[0] );
                if ( scanType.equals("movie" ) )
                {
                    updateDetails( entries[0] );
                }
                else
                {
                    updateDetailsTV( entries[0] );
                }
                */
            }
            public void widgetDefaultSelected(SelectionEvent ev) {
            }
        });

        /*
         * Build button scans new entries in our MovieDB list.  
         * It leaves existing entries unmodified.
         */
        buttons[SCANNEWBUTTON] = new Button(group, SWT.PUSH);
        buttons[SCANNEWBUTTON].setText("Scan New");
        buttons[SCANNEWBUTTON].addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent ev) {
                if ( fileListing == null )
                {
                    log.error("No file listing to scan.");
                    return;
                }
                Iterator it = fileListing.iterator();
                while( it.hasNext() )
                {
                    File entry = (File)it.next();
                    String name = entry.getName();
                    String[] exts = extensionsText.getText().split(" ");
                    for(int i=0; i<exts.length; i++)
                    {
                        name = name.replace("."+exts[i], "");
                    }
                    name = name.replace("_", " ");

                    if ( scanType.equals("movie" ) )
                    {
                        MovieDB movie = findMovie(name);
                        if ( movie == null )
                        {
                            try {
                                // TheMovieDB only allows 30 queries / 10 seconds.
                                // We put a little delay in here to deal with that.
                                movie = new MovieDB(name);
                                movies.add(movie);
                                movie.setPath(entry.getPath());
                            }
                            catch(Exception e) {
                                log.info("GeneralException: Movie setup failed", e);
                                e.printStackTrace();
                            }
                        }
                    }
                    else
                    {
                        TVDB episode = findTVDB(name);
                        if ( episode == null )
                        {
                            try {
                                episode = new TVDB(name);
                                episodes.add(episode);
                                episode.setPath(entry.getPath());
                            }
                            catch(Exception e) {
                                log.info("GeneralException: TVDB setup failed", e);
                                e.printStackTrace();
                            }
                        }
                    }
                }

                Timer time = new Timer();
                if ( scanType.equals("movie" ) )
                {
                    ScanMovies sm = new ScanMovies(false);
                    time.schedule(sm, 10);
                }
                else
                {
                    ScanTV st = new ScanTV(false);
                    time.schedule(st, 10);
                }
            }
            public void widgetDefaultSelected(SelectionEvent ev) {
            }
        });
        gd = new GridData(GridData.FILL_HORIZONTAL);
        buttons[SCANNEWBUTTON].setLayoutData(gd);

        buttons[RESCANBUTTON] = new Button(group, SWT.PUSH);
        buttons[RESCANBUTTON].setText("Rescan All");
        buttons[RESCANBUTTON].addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent ev) {
                movieList.setRedraw(false);
                movieList.removeAll();
                movieList.setRedraw(true);
                movies.clear();
                episodes.clear();
                scanByFilename();

                Timer time = new Timer();
                if ( scanType.equals("movie" ) )
                {
                    ScanMovies sm = new ScanMovies(true);
                    time.schedule(sm, 10);
                }
                else
                {
                    ScanTV st = new ScanTV(true);
                    time.schedule(st, 10);
                }
            }
            public void widgetDefaultSelected(SelectionEvent ev) {
            }
        });
        gd = new GridData(GridData.FILL_HORIZONTAL);
        buttons[RESCANBUTTON].setLayoutData(gd);

        buttons[COMPRESSBUTTON] = new Button(group, SWT.PUSH);
        buttons[COMPRESSBUTTON].setText("Compress");
        buttons[COMPRESSBUTTON].addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent ev) {
                /* 
                 * Save compression script to /tmp
                 * Then update the executable that runs the script.
                 * Then run it.
                 */
                String script = cli.get(Cli.S_COMPRESSSCRIPT);
                String scriptPath = cli.get(Cli.S_COMPRESSPATH);
                String posterPath = dirName.getText() + File.separator + "posters";
                String command = cli.get(Cli.S_COMPRESSCMD);
                command = command.replace("%s", scriptPath);
                command = command.replace("%d", posterPath);
                try { 
                    PrintWriter pw = new PrintWriter(scriptPath);
                    pw.println( script );
                    pw.close();
                    Runtime rt = Runtime.getRuntime();  
                    Process pr = rt.exec(command);
                }
                catch(Exception e) {
                    log.error("Failed to run " + command, e);
                }

                // open dialog
                // warn about how long it takes
                // start and close buttons
                // disable main window until compressions are complete
            }
            public void widgetDefaultSelected(SelectionEvent ev) {
            }
        });
        gd = new GridData(GridData.FILL_HORIZONTAL);
        buttons[COMPRESSBUTTON].setLayoutData(gd);

        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 3;
        gd.heightHint = 500;
        movieList = new org.eclipse.swt.widgets.List(group, SWT.V_SCROLL);
        movieList.setLayoutData(gd);
        movieList.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent ev) {
                String[] entries = movieList.getSelection();
                if ( (entries == null) || (entries.length == 0) )
                    return;
                if ( scanType.equals("movie" ) )
                    updateDetails(entries[0]);
                else
                    updateDetailsTV(entries[0]);
            }
            public void widgetDefaultSelected(SelectionEvent ev) {
            }
        });
    }

    /* Movie Details */
    private void buildDetailsFrame() 
    {
        GridData gd = new GridData(GridData.FILL_BOTH);
        Composite composite = new Composite(shell, SWT.NONE);
        composite.setLayout(new GridLayout(1, false));
        composite.setLayoutData(gd);

        GridLayout gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = false;
        composite.setLayoutData(gd);

        Group group = new Group(composite, SWT.NONE);
        group.setText("Details");
        group.setFont(boldFont);
        gd = new GridData(GridData.FILL_BOTH);
        group.setLayoutData(gd);

        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 4;
        gl.numColumns = 2;
        gl.makeColumnsEqualWidth = false;
        group.setLayout(gl);

        // Movie name used in search
        Label label = new Label(group, SWT.NONE);
        label.setText("File Title");
        tmdbName = new Text(group, SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        tmdbName.setLayoutData(gd);

        // Video name reported by upstream
        label = new Label(group, SWT.NONE);
        label.setText("Real Title");
        videoName = new Text(group, SWT.BORDER);
    	videoName.addListener(SWT.KeyUp, new Listener()
    	{
        	@Override
        	public void handleEvent(Event arg0)
        	{
            	if(arg0.character == SWT.CR)
                {
                    String value = videoName.getText();
                    if ( value.length() == 0 )
                        return;
                    if ( dirName.getText().length() == 0 )
                        return;

                	/* Enter key in this field causes the display title to be changed. */
                    String dirname = dirName.getText() + File.separator + "videolib";
                    log.info("dirname: " + dirname);
                    if ( scanType.equals("movie" ) )
                    {
                        String pathname=dirname + File.separator + "movies";
                        log.info("Movie pathname: " + pathname);
                        MovieDB movie = findMovie(tmdbName.getText());
                        if ( movie != null )
                        {
                            /* This feature is not enabled for Movies at this time. */
                            // movie.setRealTitle( value );
                            // movie.save(dirName.getText(), pathname);
                        }
                    }
                    else
                    {
                        String pathname=dirname + File.separator + "tv";
                        log.info("TV pathname: " + pathname);
                        TVDB episode = findTVDB(tmdbName.getText());
                        if ( episode != null )
                        {
                            log.info("Saving TV RealTitle: " + value);
                            episode.setRealTitle( value );
                            episode.save(dirName.getText(), pathname);
                        }
                    }
                }
        	}
    	});

        gd = new GridData(GridData.FILL_HORIZONTAL);
        videoName.setLayoutData(gd);

        // Release Date
        label = new Label(group, SWT.NONE);
        label.setText("Release Date");
        releaseDate = new Text(group, SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        releaseDate.setLayoutData(gd);

        label = new Label(group, SWT.NONE);
        label.setText("Overview");
        gd = new GridData(GridData.VERTICAL_ALIGN_BEGINNING);
        gd.horizontalSpan = 2;
        label.setLayoutData(gd);

        overview = new Text(group, SWT.BORDER|SWT.MULTI|SWT.WRAP|SWT.V_SCROLL);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 2;
        gd.heightHint = 200;
        gd.grabExcessHorizontalSpace = true;
        overview.setLayoutData(gd);

        label = new Label(group, SWT.NONE);
        label.setText("Alternatives");
        gd = new GridData(GridData.VERTICAL_ALIGN_BEGINNING);
        gd.horizontalSpan = 2;
        label.setLayoutData(gd);

        // Alternative titles
        // A table of titles with their associated IDs from the JSON.
        alternativesTable = new Table(group, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 2;
        gd.widthHint = 220;
        gd.grabExcessHorizontalSpace = true;
        alternativesTable.setLayoutData(gd);
        alternativesTable.setLinesVisible(true);
        alternativesTable.addListener (SWT.Selection, new Listener() {
            public void handleEvent(Event event) {
                TableItem[] tableItem = alternativesTable.getSelection();
                String altID = tableItem[0].getText(2);
                if ( scanType.equals("movie" ) )
                {
                    MovieDB movie = findMovie(tmdbName.getText());
                    movie.setID(altID);
                    updateDetails(movie.getTitle());
                }
                else
                {
                    log.info("Want alternative ID: " + altID);
                    TVDB episode = findTVDB(tmdbName.getText());
                    episode.setID(altID);
                    updateDetailsTV(episode.getTitle());
                }
            }
        });

        TableColumn tc1 = new TableColumn(alternativesTable, SWT.CENTER);
        TableColumn tc2 = new TableColumn(alternativesTable, SWT.LEFT);
        TableColumn tc3 = new TableColumn(alternativesTable, SWT.LEFT);
        tc1.pack();
        tc2.pack();
        tc3.pack();
        tc1.setWidth(80);
        tc2.setWidth(220);
        tc3.setWidth(0);
    }

    /* Movie Poster */
    private void buildPosterFrame() 
    {
        Composite composite = new Composite(shell, SWT.NONE);
        composite.setLayout(new GridLayout(1, false));

        GridData gd = new GridData(GridData.FILL_BOTH);
        gd.widthHint = 400;
        gd.heightHint = 800;
        gd.grabExcessVerticalSpace = true;
        composite.setLayoutData(gd);

        /*
         * A viewform to give us multiple pages, 
         * one for movie posters and 
         * one for TV episode images with their season image.
         */
        viewForm = new ViewForm(composite, SWT.NONE);
        viewForm.pack();

        /*
         * Movie poster
         */
        Group group = new Group(viewForm, SWT.NONE);
        group.setText("Movie Poster");
        group.setFont(boldFont);
        gd = new GridData(GridData.FILL_BOTH);
        group.setLayoutData(gd);
        viewFormContent[MOVIE_TAB] = group;

        GridLayout gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = false;
        group.setLayout(gl);

        // Poster art
        posterLabel = new Label(group, SWT.NONE);
        gd = new GridData(GridData.FILL_BOTH);
        gd.heightHint = 800;
        gd.grabExcessVerticalSpace = true;
        posterLabel.setLayoutData(gd);

        Image image = new Image(shell.getDisplay(), 400,800);
        GC gc = new GC(image);
        Color white = Display.getDefault().getSystemColor(SWT.COLOR_WHITE);
        gc.setForeground(white);
        gc.drawRectangle(0,0,400,800);
        gc.dispose();
        posterLabel.setImage(image);

        /*
         * TV series and episode images. 
         */
        group = new Group(viewForm, SWT.NONE);
        group.setText("Series and Episode Posters");
        group.setFont(boldFont);
        gd = new GridData(GridData.FILL_BOTH);
        group.setLayoutData(gd);
        viewFormContent[TV_TAB] = group;

        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = false;
        group.setLayout(gl);

        // Series art
        seriesLabel = new Label(group, SWT.NONE);
        gd = new GridData(GridData.FILL_BOTH);
        gd.heightHint = 400;
        gd.grabExcessVerticalSpace = true;
        seriesLabel.setLayoutData(gd);

        image = new Image(shell.getDisplay(), 400,400);
        gc = new GC(image);
        gc.setForeground(white);
        gc.drawRectangle(0,0,400,400);
        gc.dispose();
        seriesLabel.setImage(image);

        // Episode art
        episodeLabel = new Label(group, SWT.NONE);
        gd = new GridData(GridData.FILL_BOTH);
        gd.heightHint = 400;
        gd.grabExcessVerticalSpace = true;
        episodeLabel.setLayoutData(gd);
        image = new Image(shell.getDisplay(), 400,400);

        gc = new GC(image);
        gc.setForeground(white);
        gc.drawRectangle(0,0,400,400);
        gc.dispose();
        episodeLabel.setImage(image);

        /*
         * Set the default to the movies group.
         */
        viewForm.setContent( viewFormContent[MOVIE_TAB] );
    }

    /*
     * Initialize the display components
     */
    private void buildUI() 
    {
        display = new Display();
        shell = new Shell(display);
        shell.setText("VideoLib for PiBox");

        shell.addListener(SWT.Close, new Listener() {
            public void handleEvent(Event event) {
                Cli cli = new Cli();
                cli.disable();
            }
        });

        boldFont = new Font(Display.getDefault(), "Tahoma", 10, SWT.BOLD);
        mediumFont = new Font(Display.getDefault(), "Tahoma", 9, SWT.ITALIC);
        disclaimerFont = new Font(Display.getDefault(), "Courier", 8, SWT.NONE);
        red = new Color(Display.getDefault(), 250, 150, 150);

        // Add a menu bar across the top
        Menu menuBar = new Menu (shell, SWT.BAR);
        shell.setMenuBar ( menuBar );

        /* Add File Menu */
        MenuItem menuBar_File= new MenuItem (menuBar, SWT.CASCADE);
        menuBar_File.setText ("&File");
        Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
        menuBar_File.setMenu( fileMenu );
        for (int i=0; i<FileMenuItems.length; i++)
        {
            final int idx = i;
            MenuItem item = new MenuItem (fileMenu, SWT.CASCADE);
            item.setText( FileMenuItems[i] );
            item.addListener (SWT.Selection, new Listener () {
                public void handleEvent (Event e) {
                    fileMenuHandler(idx);
                }
            });
        }

        /* Add Config Menu */
        MenuItem menuBar_Config= new MenuItem (menuBar, SWT.CASCADE);
        menuBar_Config.setText ("&Config");
        Menu configMenu = new Menu(shell, SWT.DROP_DOWN);
        menuBar_Config.setMenu( configMenu );
        for (int i=0; i<ConfigMenuItems.length; i++)
        {
            final int idx = i;
            MenuItem item = new MenuItem (configMenu, SWT.CASCADE);
            item.setText( ConfigMenuItems[i] );
            item.addListener (SWT.Selection, new Listener () {
                public void handleEvent (Event e) {
                    configMenuHandler(idx);
                }
            });
        }

        /* Add Help Menu */
        MenuItem menuBar_Help= new MenuItem (menuBar, SWT.CASCADE);
        menuBar_Help.setText ("&Help");
        Menu helpMenu = new Menu(shell, SWT.DROP_DOWN);
        menuBar_Help.setMenu( helpMenu );
        for (int i=0; i<HelpMenuItems.length; i++)
        {
            final int idx = i;
            MenuItem item = new MenuItem (helpMenu, SWT.CASCADE);
            item.setText( HelpMenuItems[i] );
            item.addListener (SWT.Selection, new Listener () {
                public void handleEvent (Event e) {
                    helpMenuHandler(idx);
                    // int style = SWT.ICON_INFORMATION | SWT.OK;
                    // MessageBox dia = new MessageBox(shell, style);
                    // dia.setText("About VideoLib...");
                    // dia.setMessage(about); 
                    // dia.open();
                }
            });
        }

        // Create a grid below the menu bar
        GridLayout gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 3;
        gl.makeColumnsEqualWidth = false;
        shell.setLayout(gl);

        // The List Frame
        buildListFrame();

        // The Details Frame
        buildDetailsFrame();

        // The Poster Frame
        buildPosterFrame();

        shell.pack();
    }

    /*
     * =======================================================
     * Public Methods - Thread Managmenet
     * =======================================================
     */

    /** 
     * Setup specific setup items.
     */
    static String getExts() 
    {
        if ( extensionsText != null )
            return extensionsText.getText();
        return null;
    }

    /** 
     * Setup specific setup items.
     */
    boolean setup() 
    {
        if ( !isRunning )
        {
            return true;
        }
        return false;
    }

    /** 
     * Start the thread.
     * @return True if the thread is started, false if it is already running.
     * @throws IOException if the thread cannot start.
     */
    synchronized boolean start() throws IOException
    {
        /*
         * Create and start the thread.
         */
        if ( !isRunning )
        {
            // Start UI thread
            this.thread = new Thread(this, "VLIBUI");
            thread.start();

            /* Wait for thread to start. */
            Calendar startWait = Calendar.getInstance();
            while ( !isRunning )
            {
                Calendar now = Calendar.getInstance();
                now.add(Calendar.SECOND, (-1*5));
                if ( now.after(startWait) )
                    break;
            }
            if ( !isRunning )
            {
                thread.interrupt();
                thread = null;
                runComplete = false;
                throw new IOException("Timed out waiting for server thread to start.");
            }
            return true;
        }
        else
            return false;
    }

    /** 
     * Stop the thread.  This always succeeds and can be used to make
     * sure the thread is ready to be started again.
     */
    synchronized void shutdown()
    {
        doShutdown = true;
        if ( isRunning )
        {
            isRunning = false;
            try {
                thread.interrupt();

                /* Wait for the thread to stop. */
                Calendar startWait = Calendar.getInstance();
                while ( !runComplete )
                {
                    Calendar now = Calendar.getInstance();
                now.add(Calendar.SECOND, (-1*5));
                    if ( now.after(startWait) )
                        break;
                }
            }
            catch(Exception e) { }
            log.info("UI shutdown complete.");
        }
        doShutdown = false;
    }

    /** 
     * Handle inbound messages.
     */
    public void run()
    {
        if ( isRunning )
            return;
        runComplete = false;

        // Build the UI 
        buildUI();

        // Set visible fields
        Cli cli = new Cli();
        if ( cli.get(Cli.S_APIKEY) != null )
            apiText.setText( cli.get(Cli.S_APIKEY) );

        // Display the db, if one exists.
        if ( cli.get(Cli.S_MOVIEDIR) != null )
            dirName.setText( cli.get(Cli.S_MOVIEDIR) );

        /*
         *----------------------------------------------------------------
         * Spin, waiting on shutdown.
         *----------------------------------------------------------------
         */
        log.info("Starting UI Thread.");
        isRunning = true;
        shell.open();
        while ( isRunning && !shell.isDisposed() )
        {
            if ( !display.readAndDispatch() )
                display.sleep();
        }
        if ( !shell.isDisposed() )
            shell.dispose();
        display.dispose();
        runComplete = true;
        log.info("UI thread exiting.");
    }
}
