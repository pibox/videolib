package com.ximba.videolib.taskdefs;

import java.io.*;
import java.util.*;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.BuildException;

/**
 * <p> 
 * Parse the specified file, replacing the specified tags with the values provided.
 * </p>
 * @author Michael J. Hammel
 * @since 4.0
 */

public class ParseFileTags extends Task {

    /** The nested tag/values */
    List<Tag> tags = new ArrayList<Tag>();

    /** Source file to be read. */
    String srcFilename = null;

    /** Destination file to be written. */
    String destFilename = null;

    /** Destination file to be written. */
    boolean overwrite = true;

    /*
     * =======================================================
     * Inner classes
     * =======================================================
     */

    /* A class to hold an instance of a name/value pair. */
    public class Tag {
        String name;
        String value;
        public Tag() { }
        public void setName(String t) { name = t; }
        public void setValue(String v) { value = v; }
        public String getName() { return name; }
        public String getValue() { return value; }
    }


    /*
     * =======================================================
     * Private methods.
     * =======================================================
     */

    private String readFile( String filename ) throws Exception
    {
        StringBuilder lines = new StringBuilder("");

        try {
            FileReader fr = new FileReader(filename);
            BufferedReader in = new BufferedReader(fr);
            String line = null;
            while ( (line=in.readLine()) != null )
                lines.append(line + "\n");
            fr.close();
            return lines.toString();
        }
        catch (Exception e)
        {
            throw new Exception("Can't read source file: " + filename);
        }
    }

    private void writeFile( String filename, String text ) throws Exception
    {
        try {
            File file = new File( filename );
            file.createNewFile();
            FileWriter fw = new FileWriter(file);
            PrintWriter pw = new PrintWriter(fw);
            pw.println(text);
            fw.close();
        }
        catch (Exception e)
        {
            throw new Exception("Can't write to destination file: " + filename);
        }
    }


    /*
     * =======================================================
     * Public methods.
     * =======================================================
     */

    /*
     * We must have the name of the file to parse and the output file. 
     */
    public void setSource( String path ) { srcFilename = path; }
    public void setDestination( String path ) { destFilename = path; }
    public void setOverwrite( boolean o ) { overwrite = o; }

    /* Nested tag elements are created here when called by Ant. */
    public Tag createTag() {
        Tag tag = new Tag();
        tags.add(tag);
        return tag;
    }


    /* Main executation method - called from Ant. */
    public void execute() {

        /* Error checking. */
        if (srcFilename == null) 
            throw new BuildException("Source filename parameter is required.");
        if (destFilename == null) 
            throw new BuildException("Destination filename parameter is required.");

        File destFile = new File(destFilename);
        if ( destFile.exists() && !overwrite )
            throw new BuildException("Destination file exists but overwrite is not enabled.");

        File file = new File(srcFilename);
        if ( !file.exists() )
            throw new BuildException("File does not exist: " + srcFilename);

        if ( tags.size() == 0 )
            throw new BuildException("No tags specified.  File was not parsed: " + srcFilename);


        /* Read the source file into a single string. */
        String fileText;
        try { fileText = readFile(srcFilename); }
        catch (Exception e) { throw new BuildException(e.getMessage()); }

        /* For each tag, replace each occurance in the source text. */
        for (Iterator it = tags.iterator(); it.hasNext(); )
        {
            Tag tag = (Tag)it.next();
            fileText= fileText.replaceAll("\\[--"+tag.getName()+"--\\]", tag.getValue());
        }

        /* Write the updated text to the destination file. */
        try { writeFile(destFilename, fileText); }
        catch (Exception e) { throw new BuildException(e.getMessage()); }
    }
}

