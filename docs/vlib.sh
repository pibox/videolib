#!/bin/bash -p
# Functions and aliases used to nagivate project directories.
# Include by the ~/bin/cdtools script.
#############################################################################

# -------------------------------------------------------------------
# DESC: VideoLib - Java app for searching for movie data on TheMovieDB.org
# Edit variables with <> values, as needed.
# -------------------------------------------------------------------
function vlib {
    # -------------------------------------------------------------------
    # Edit these items
    # -------------------------------------------------------------------

    # Top of the source tree
    # The project directory will be placed under SRCTOP
    SRCTOP=<PATH TO WHERE PROJECT DIRECTORY WILL LIVE>

    # Java library
    # Point this to where Java is installed.  
    # Most Linux systems shouldn't need to change this if the
    # Sun/Oracle Java 1.6 release (recommended) is used.  
    JAVA_HOME=/usr/java/default/

    # -------------------------------------------------------------------
    # Don't edit below here
    # -------------------------------------------------------------------

    # Project name
    PARENT=pibox
    PRJ=videolib

    # If supplied, the second argument is a suffix, to allow multiple versions of the same tree.
    SFX=$1

    # Repository
    export GITREPO=git@gitlib.com:$PARENT/videolib.git

    # Create top level directory, if needed
    mkdir -p $SRCTOP

    # Where I do my dev work
    GM_WORK=$SRCTOP/work
    mkdir -p $GM_WORK

    # Where the SCM is located
    GM_HOME=$SRCTOP/$PRJ$SFX

    # Where the source and build directories live
    GM_SRC=$GM_HOME/src
    GM_BUILD=$GM_HOME/bld

    # Archives, packaging extras
    GM_ARCHIVE=$GM_HOME/archive
    GM_PKG=$GM_HOME/pkg
    GM_EXTRAS=$GM_HOME/extras

    # Make the configured environment available 
    export GM_WORK
    export GM_ARCHIVE
    export GM_HOME
    export GM_SRC
    export GM_BUILD
    export GM_PKG
    export GM_EXTRAS

    # Some aliases to bounce around directories easily
    alias cdt='cd $SRCTOP'
    alias cdh='cd $GM_HOME'
    alias cdw='cd $GM_WORK'
    alias cdx='cd $GM_SRC'
    alias cdb='cd $GM_BUILD'
    alias cdp='cd $GM_PKG'
    alias cda='cd $GM_ARCHIVE'
    alias cde='cd $GM_EXTRAS'

    # Show the aliases for this configuration
    alias cd?='listvideolib'
}
function listvideolib {
echo "
$PRJ Alias settings:
-----------------------------------------------------------------------------
cdt    cd SRCTOP ($SRCTOP)
cdh    cd GM_HOME ($GM_HOME)
cdw    cd GM_WORK ($GM_WORK)
cdx    cd GM_SRC ($GM_SRC)
cdb    cd GM_BUILD ($GM_BUILD)
cdp    cd GM_PKG ($GM_PKG)
cda    cd GM_ARCHIVE ($GM_ARCHIVE)
cde    cd GM_EXTRAS ($GM_EXTRAS)

Repository: $REPO

To checkout tree:
cdt
mkdir $PRJ$SFX
cdh
git clone $GITREPO src
"
}
