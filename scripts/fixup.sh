#!/bin/bash
# Convert and resize poster images.
# First positional argument is the directory 
# where the posters are stored.
# ---------------------------------------------

if [ "$1" = "" ]
then
    echo "Missing poster directory: $1"
    sleep 10
    exit 1
fi
if [ ! -d $1 ]
then
    echo "No such directory: $1"
    sleep 10
    exit 1
fi

cd $1
pwd
sleep 3
for file in `ls -1`
do
    prefix=`echo $file | cut -f1 -d"."`
    orig=`ls -l $file`
    echo "--- Processing $file "
    convert $file $prefix.png
    convert $prefix.png -resize 400x400 $prefix.new.png
    optipng -o7 $prefix.new.png
    rm -f $prefix.png
    mv $prefix.new.png $file
    echo "$orig"
    ls -l $file
done
sleep 10
