%define rev 1
%define prjdir opt/videolib

Name: videolib
Summary: Generate local database of videos
Version: [--VERSION--]
Release: %{rev}
Source: %{name}-%{version}.tar.gz
Vendor: Michael J. Hammel
License: Proprietary
URL: https://gitlab.com/pibox/videolib
Packager: Michael J. Hammel <mjhammel@graphics-muse.org>

# Group is based on the Fedora Groups hierarchy.  See
# /usr/share/doc/rpm-<version>/GROUPS for details.
Group: Applications/Engineering

AutoReqProv: no
Requires: jdk
BuildRoot: %{_tmppath}/%{name}-root

%description
<project description>

%prep
%setup
                                                                                
%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/%{prjdir}
mkdir -p $RPM_BUILD_ROOT/usr/bin

cp -r videolib/* $RPM_BUILD_ROOT/%{prjdir}
ln -s /%{prjdir}/scripts/VideoLib.sh $RPM_BUILD_ROOT/usr/bin/VideoLib

%clean
rm -rf $RPM_BUILD_ROOT

%files
%attr(0755,root,root) /%{prjdir}
/usr/bin/VideoLib
/%{prjdir}/scripts/VideoLib.sh
