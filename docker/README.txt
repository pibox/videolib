===============================================================================
QUICK START
===============================================================================

1. Build image:
    Use the build.sh script.  See ./build.sh -? for usage details.

2. Run an image:
   sudo docker run -it --rm --privileged=true -v /home/$(whoami):/home/$(whoami) -v /usr/local:/usr/local pibox_build

The use of /usr/local is for installing a verison of opkg for app building which will be done outside the docker environment.
Add additional mountpoints inside the docker image with the -v option.

The --privileged=true option allows the container to use loopback devices and to mount 
filesystems which is required when running various -pkg targets of the build.

For machines that have SELinux turned on you may need to add ":z" to the end of
the -v argumments to get permissions set correctly. For example, the home mount
would change to: "-v /home/$(whoami):/home/$(whoami):z"

===============================================================================
USE
===============================================================================
Starting the docker image will place you in the home directory of the specified
user, which is normally your home directory.  You will need to change into the 
directory containing PiBox to run builds.

See docs/bashsetup.sh and use cdtools to manage source, build and packaging
dirctories.

cdtools: https://www.graphics-muse.org/wiki/pmwiki.php/Cdtools/Cdtools
