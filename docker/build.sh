#!/bin/bash
# Build a Docker environment suitable for building the
# PiBox Development Platform.
#--------------------------------------------------------------

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
# Prefix with "P" to avoid namespace collisions.
PUID="$(id -u)"
PGID="$(id -g)"
PUNAME="$(id -u -n)"
PW=""
BLDNAME="pibox_build"

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------

# Provide command line usage assistance
doHelp()
{
    echo ""
    echo "$0 [-b name | -u uid | -g gid | -n username] -p pw"
    echo "where"
    echo "-b name     Build name.  Defaults to ${BLDNAME}."
    echo "-p pw       Required: Password to use when setting up user in Docker image."
    echo "-u uid      Numeric user id; Defaults to your uid: $(id -u)"
    echo "-g uid      Numeric group id; Defaults to your gid: $(id -g)"
    echo "-n username User nam; Defaults to your username: $(id -u -n)"
    echo ""
    echo "See the README.txt for detailed build and use instructions for the PiBox docker image."
}

# Validate a string is numeric
notANum()
{
    value=$1
    re='^[0-9]+$'
    if ! [[ ${value} =~ ${re} ]] ; then
        return 0
    fi
    return 1
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":b:g:n:p:u:" Option
do
    case $Option in
    b) BLDNAME="${OPTARG}";;
    g) PGID="${OPTARG}";;
    n) PUNAME="${OPTARG}";;
    p) PW="${OPTARG}";;
    u) PUID="${OPTARG}";;
    *) doHelp; exit 0;;
    esac
done

# Validate password
if [[ -z ${PW} ]]; then
    echo "Password option is required."
    doHelp
    exit 1
fi

# Validate uid
if notANum "${PUID}"; then
    echo "UID must be numeric."
    doHelp
    exit 1
fi

# Validate gid
if notANum "${PGID}"; then
    echo "GID must be numeric."
    doHelp
    exit 1
fi

# Validate username
if [[ -z ${PUNAME} ]]; then
    echo "Username must not be empty string."
    doHelp
    exit 1
fi

#--------------------------------------------------------------
# Main
#--------------------------------------------------------------
sudo docker build \
    --build-arg UID="${PUID}" \
    --build-arg GID="${PGID}" \
    --build-arg UNAME="${PUNAME}" \
    --build-arg PW="${PW}" \
    -t ${BLDNAME} ./
