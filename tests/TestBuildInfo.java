package com.ximba.videolib;

// TestNG
import org.testng.annotations.*;

// Java
import org.apache.log4j.*;

// videolib
import com.ximba.common.BuildInfo;

/**
 * Tests for BuildInfo class.
 */
@Test(groups="utils")
public class TestBuildInfo
{
    private final Logger log = Logger.getLogger("TestBuildInfo");
    Setup setup = new Setup();

    /*
     * ------------------------------------------------------------
     * Before/After methods
     * ------------------------------------------------------------
     */
    @BeforeClass
    private void beforeClass() throws Exception
    {
        setup.init();
        setup.setupLog();
    }

    @AfterClass
    private void afterClass() throws Exception
    {
        setup.shutdownLog();
    }

    @AfterMethod
    private void afterMethod() throws Exception
    {
        // Teardown after each test.
        setup.cleanup();
    }

    /*
     * ------------------------------------------------------------
     * Tests
     * ------------------------------------------------------------
     */
    @Test()
    public void isSystemNameSet()
    {
        log.info("--- bi1 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("bi1") )
            return;

        assert ( !BuildInfo.SYSTEM_NAME.equals("[--NAME--]") );
    }

    @Test()
    public void isVersionSet()
    {
        log.info("--- bi2 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("bi2") )
            return;

        assert ( !BuildInfo.VERSION.equals("[--VERSION--]") );
    }

    @Test()
    public void isBuildIDSet()
    {
        log.info("--- bi3 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("bi3") )
            return;

        assert ( !BuildInfo.BUILD_ID.equals("[--BUILD_ID--]") );
    }

}
