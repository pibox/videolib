package com.ximba.videolib;

// Java
import java.lang.reflect.*;
import java.util.*;
import org.apache.log4j.*;

// TestNG
import org.testng.annotations.*;

// videolib
import com.ximba.common.*;
import com.ximba.videolib.Cli;

/**
 * Tests for Cli class.
 */
@Test(groups="cli")
public class TestCli
{
    private final Logger log = Logger.getLogger("TestCmd");
    Setup setup = new Setup();

    Class   cls     = null;
    Field   field   = null;
    Cli     cli     = null;

    /*
     * ------------------------------------------------------------
     * Before/After methods
     * ------------------------------------------------------------
     */

    @BeforeClass
    private void beforeClass() throws Exception
    {
        setup.init();
        setup.setupLog();
    }

    @AfterClass
    private void afterClass() throws Exception
    {
        setup.shutdownLog();
    }

    @BeforeMethod
    @SuppressWarnings("unchecked")
    private void beforeMethod() throws Exception
    {
        // Create new instance and clear configuration object
        cli = new Cli();
        cls = cli.getClass();
        field = cls.getDeclaredField("cfg");
        field.setAccessible(true);
        Map<String, String> cfg = (HashMap<String, String>)field.get(cli);
        cfg.clear();

        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field = cls.getDeclaredField("runDir");
        field.setAccessible(true);
        field.set(cli, null);
    }

    @AfterMethod
    private void afterMethod() throws Exception
    {
        // Teardown after each test.
        setup.cleanup();
    }

    /*
     * ------------------------------------------------------------
     * Tests
     * ------------------------------------------------------------
     */

    @Test(description="parseCmdLine")
    public void cli1()
    {
        log.info("--- cli1 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("cli1") )
            return;

        String[] args = null;
        try { cli.parseCmdLine(args); }
        catch(Exception e) {
            assert false: "Failed with null args.";
        }

        args = new String[1];
        args[0] = "blah";
        try { cli.parseCmdLine(args); }
        catch(Exception e) {
            assert false: "Exception with unknown args.";
        }

        args[0] = "-verbose";
        try { cli.parseCmdLine(args); }
        catch(Exception e) {
            assert false: "Exception with unknown args.";
        }
    }

    // get
    @Test(description="get a config item")
    public void cli2()
    {
        log.info("--- cli2 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("cli2") )
            return;

        String[] args = null;
        try { cli.parseCmdLine(args); }
        catch(Exception e) {
            assert false: "Failed with null args.";
        }
        String val = Cli.get("enabled");
        assert (val != null): "get() failed.";
    }

    // Enable
    @Test(description="enable")
    public void cli3()
    {
        log.info("--- cli3 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("cli3") )
            return;

        cli.enable();
        String val = Cli.get("enabled");
        assert (val != null): "get() failed.";
        assert val.equals("true"): "Enabled not set properly.";
    }

    // Disable
    @Test(description="disable")
    public void cli4()
    {
        log.info("--- cli4 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("cli4") )
            return;

        cli.enable();
        cli.disable();
        String val = Cli.get("enabled");
        assert (val != null): "get() failed.";
        assert val.equals("false"): "Enabled not reset properly.";
    }

    // isVerbose
    @Test(description="isVerbose")
    public void cli5()
    {
        log.info("--- cli5 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("cli5") )
            return;

        String[] args = null;
        try { cli.parseCmdLine(args); }
        catch(Exception e) {
            assert false: "Failed with null args.";
        }

        String val = Cli.get("verbose");
        assert (val != null): "get() failed.";
        assert val.equals("false"): "Verbose not initialized properly.";
        assert !Cli.isVerbose(): "Verbose failed disabled check.";

        try { 
            beforeMethod();
            args = new String[1];
            args[0] = "-verbose";
            cli.parseCmdLine(args); 
        }
        catch(Exception e) {
            assert false: "Exception with unknown args.";
        }
        assert Cli.isVerbose(): "Verbose failed enabled check.";
    }

    // isEnabled
    @Test(description="isEnabled")
    public void cli6()
    {
        log.info("--- cli6 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("cli6") )
            return;

        String[] args = null;
        try { cli.parseCmdLine(args); }
        catch(Exception e) {
            assert false: "Failed with null args.";
        }

        String val = Cli.get("enabled");
        assert (val != null): "get() failed.";
        assert val.equals("false"): "Enabled not initialized properly.";
        assert !Cli.isEnabled(): "Enabled failed disabled check.";
        cli.enable();
        assert Cli.isEnabled(): "Enabled failed enable check.";
        cli.disable();
        assert !Cli.isEnabled(): "Enabled failed reset check.";
    }
}
