package com.ximba.videolib;

// Java
import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.log4j.*;
import org.apache.log4j.spi.LoggingEvent;

// videolib
import com.ximba.common.*;
import com.ximba.videolib.*;

/**
 * Setup class for all tests.
 */
public class Setup
{
    private final Logger log = Logger.getLogger("Setup");

    LogAppender logAppender  = null;
    LogCB       logCB        = null;
    boolean     doCleanup    = true;
    String      logPrefix    = null;

    final StringBuilder msgs = new StringBuilder("\n");
    final List<String> ignore = new ArrayList<String>();

    /*
     * ------------------------------------------------------------
     * Inner class for processing inbound log messages.
     * If logPrefix is set, search only for messages with that prefix.
     * If ignore array is not empty, use it to further weed out messages.
     * ------------------------------------------------------------
     */
    public class LogCB implements LogCallback
    {
        public void append(LoggingEvent event)
        {
            String msg = (String)logAppender.getLayout().format(event);
            String[] strRep = event.getThrowableStrRep();

            String debugEnabled = System.getenv("DEBUG");
            if ( debugEnabled != null ) 
            {
                System.err.println(msg);
                if (strRep != null)
                {
                    for (int i=0; i<strRep.length; i++)
                        System.err.println(strRep[i]);
                }
            }

            if ( logPrefix != null )
            {
                if ( logPrefix.equalsIgnoreCase("any") || msg.startsWith(logPrefix) )
                {   
                    Iterator it = ignore.iterator();
                    boolean ignoreIt = false;
                    while ( it.hasNext() )
                    {   
                        String tag = (String)it.next();
                        if (msg.contains(tag) )
                        {   
                            ignoreIt = true;
                            break;
                        }
                    }
                    if ( !ignoreIt && !logPrefix.equalsIgnoreCase("any") )
                        msgs.append( msg.replace(logPrefix, "--") + "\n" );
                    else if ( !ignoreIt )
                        msgs.append( "-- " + msg + "\n" );
                }
            }
        }
    }

    /*
     * =======================================================
     * Getters
     * =======================================================
     */
    public LogAppender getLogAppender(){ return logAppender; }
    public LogCB getLogCB(){ return logCB; }

    /*
     * =======================================================
     * Public Methods
     * =======================================================
     */

    /*
     * Reinit this class.
     */
    public void init()
    {
        clearLog();
    }

    /*
     * Setup logging - used only when DEBUG is set in the environment.
     */
    public void setupLog()
    {
        logCB = new LogCB();
        LogAppender.register( logCB );

        Logger rootLogger = Logger.getRootLogger();
        rootLogger.removeAllAppenders();
        rootLogger.setLevel(Level.INFO);
        logAppender = new LogAppender();
        logAppender.setLayout( new PatternLayout("[%t] %-5p %c{2} %m") );
        rootLogger.addAppender(logAppender);
    }

    /*
     * Teardown logging.
     */
    public void shutdownLog()
    {
        LogAppender.remove( logCB );
        Logger rootLogger = Logger.getRootLogger();
        rootLogger.removeAllAppenders();
    }

    /*
     * Set a prefix for logger to search for.
     */
    public void setPrefix( String prefix )
    {
        logPrefix = prefix;
    }

    /*
     * Add an ignore string for the logger to weed out.
     */
    public void addIgnore( String tag )
    {
        ignore.add( tag );
    }

    /*
     * Clear ignore list.
     */
    public void clearLog()
    {
        ignore.clear();
        msgs.delete(0, msgs.length());
    }

    /*
     * Retrieve the saved logs.
     */
    public String getLog()
    {
        return msgs.toString();
    }

    /*
     * Disable cleanup - this prevents cleanup from being performed in afterMethod's.
     */
    public void disableCleanup()
    {
        doCleanup = false;
    }

    /*
     * Remove directories created by setupAC().
     */
    public void cleanup()
    {
        if ( !doCleanup )
            return;
    }
}
