#!/bin/bash
# Create a directory listing matching a video tree.
# This just recreates the directory structure and filenames.
# It doesn't copy the files.
# 
# Run this script from the tests directory.  The structure
# will be created under "files".
# -----------------------------------------------------------

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
MDIR=/movies/Cinema-3/Mobile/tv
DOCLEAN=0

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
# Provide command line usage assistance
function doHelp
{
    echo ""
    echo "$0 [-c | -p path ]"
    echo "where"
    echo "-c          Clean the destination directory and exit."
    echo "-p path     Path to copy from."
    echo "            Default: $MDIR"
    echo ""
    echo "Create a directory listing matching a video tree."
    echo "This just recreates the directory structure and filenames."
    echo "It doesn't copy the files."
    echo ""
    echo "Run this script from the tests directory.  The structure"
    echo "will be created under \"files\"."
    echo ""
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":cp:" Option
do
    case $Option in
    c) DOCLEAN=1;;
    p) MDIR=$OPTARG;;
    *) doHelp; exit 0;;
    esac
done

#--------------------------------------------------------------
# Main
#--------------------------------------------------------------
if [ $DOCLEAN -eq 1 ]
then
    rm -rf files
    exit 0
fi

# Create the destination directory and save its path.
mkdir files
cd files
CWD=`pwd`

# Copy the path of all files in the source tree to the desintation.
cd $MDIR
find . -type f | while read file 
do
    dir="$(dirname "$file")"
    filename="$(basename "$file")"
    subdir="$(basename "$dir")"

    # echo "dir: $dir"
    # echo "filename: $filename"
    # echo "subdir: $subdir"

    # Skip items we don't need for testing
    if [[ "$subdir" == "posters" ]] || [[ "$filename" == ".videolib.db" ]] || [[ "$dir" =~ .*videolib.* ]]
    then
        echo "Skipping $file"
        continue
    fi

    echo "dir: $dir -- File: $filename"

    mkdir -p "$CWD/$dir"
    touch "$CWD/$dir/$filename"
done
