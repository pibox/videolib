package com.ximba.videolib;

// TestNG
import org.testng.annotations.*;

// Java
import org.apache.log4j.*;

// videolib
import com.ximba.common.RandomGUID;

/**
 * Tests for RandomGUID class.
 */
@Test(groups="utils")
public class TestRandomGUID
{
    private final Logger log = Logger.getLogger("TestRandomGUID");
    Setup setup = new Setup();

    /*
     * ------------------------------------------------------------
     * Before/After methods
     * ------------------------------------------------------------
     */
    @BeforeClass
    private void beforeClass() throws Exception
    {
        setup.init();
        setup.setupLog();
    }

    @AfterClass
    private void afterClass() throws Exception
    {
        setup.shutdownLog();
    }

    @AfterMethod
    private void afterMethod() throws Exception
    {
        // Teardown after each test.
        setup.cleanup();
    }

    /*
     * ------------------------------------------------------------
     * Tests
     * ------------------------------------------------------------
     */
    @Test()
    public void isNotNull()
    {
        log.info("--- rg1 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("rg1") )
            return;

        String val = new RandomGUID().toString();
        assert ( val != null );
    }

    @Test()
    public void isComplete()
    {
        log.info("--- rg2 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("rg2") )
            return;

        String val = new RandomGUID().toString();
        assert ( val.length() == 36 );
    }

    @Test()
    public void secureIsNotNull()
    {
        log.info("--- rg3 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("rg3") )
            return;

        String secure = new RandomGUID(true).toString();
        assert ( secure != null );
    }

    @Test()
    public void secureIsComplete()
    {
        log.info("--- rg4 ---");
        String value = System.getenv("SKIP");
        if ( (value!=null) && value.contains("rg4") )
            return;

        String secure = new RandomGUID(true).toString();
        assert ( secure.length() == 36 );
    }
}
